﻿namespace Todo_services.Interfaces
{
    using Todo_domain_entities;

    /// <summary>
    /// To-do list management interface.
    /// </summary>
    public interface IToDoListSevices
    {
        /// <summary>
        /// Does ToDoList exist by id.
        /// </summary>
        /// <param name="idToDoList">id To-Do list.</param>
        /// <returns>true if exists, otherwise false.</returns>
        Task<bool> IsToDoListExistsAsync(long idToDoList);

        /// <summary>
        /// Add new ToDoList.
        /// </summary>
        /// <param name="toDoList">ToDoList.</param>
        /// <exception cref="ArgumentNullException">if list is null.</exception>
        /// <returns>id of new list.</returns>
        Task<long> AddListAsync(ToDoList toDoList);

        /// <summary>
        /// Show All ToDoList.
        /// </summary>
        /// <returns>All ToDoList.</returns>
        Task<IQueryable<ToDoList>> GetAllListsAsync();

        /// <summary>
        /// Update ToDoList by id.
        /// </summary>
        /// <param name="toDoList">ToDoList.</param>
        /// <exception cref="ArgumentNullException">if list is null.</exception>
        /// <returns>Task.</returns>
        Task UpdateListAsync(ToDoList toDoList);

        /// <summary>
        /// Copy ToDoList.
        /// </summary>
        /// <param name="id">id ToDoList.</param>
        /// <returns>id of new list.</returns>
        Task<long> CopyListAsync(long id);

        /// <summary>
        /// Hide or show a list in an application's list view.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <returns>Task.</returns>
        Task ChangeVisibilityAsync(long id);

        /// <summary>
        /// Delete list by id.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <returns>Task.</returns>
        Task DeleteListAsync(long id);
    }
}
