﻿namespace Todo_services.Interfaces
{
    using Todo_domain_entities;

    /// <summary>
    /// Manage of To-Do lists.
    /// </summary>
    public interface IToDoListTaskSevices
    {
        /// <summary>
        /// Cange task status.
        /// </summary>
        /// <param name="idTask">id Task.</param>
        /// <param name="newStatus">new status.</param>
        /// <returns>Task.</returns>
        Task ChangeStatusAsync(long idTask, Status newStatus);

        /// <summary>
        /// Does Task Id Exists.
        /// </summary>
        /// <param name="idTask">id Task.</param>
        /// <returns>true if exist, otherwise false.</returns>
        Task<bool> IsTaskExistsAsync(long idTask);

        /// <summary>
        /// Add new task.
        /// </summary>
        /// <param name="toDoListTask">task.</param>
        /// <exception cref="ArgumentNullException">if toDoListTask is null.</exception>
        /// <returns>Task.</returns>
        Task AddTaskAsync(ToDoListTask toDoListTask);

        /// <summary>
        /// All tasks.
        /// </summary>
        /// <returns>tasks.</returns>
        Task<IQueryable<ToDoListTask>> GetAllTasksAsync();

        /// <summary>
        /// Update task by id.
        /// </summary>
        /// <param name="toDoListTask">task.</param>
        /// <exception cref="ArgumentNullException">if toDoListTask is null.</exception>
        /// <returns>Task.</returns>
        Task UpdateTaskAsync(ToDoListTask toDoListTask);

        /// <summary>
        /// Delete task by id.
        /// </summary>
        /// <param name="idTask">task id.</param>
        /// <returns>Task.</returns>
        Task DeleteTaskAsync(long idTask);

        /// <summary>
        /// Set reminder.
        /// </summary>
        /// <param name="idTask">id Task.</param>
        /// <param name="remind">remind time.</param>
        /// <returns>Task.</returns>
        Task SetReminderAsync(long idTask, DateTime remind);
    }
}
