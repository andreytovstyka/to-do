﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace Todo_services
{
    /// <summary>
    /// Helpers.
    /// </summary>
    public static class ServicesHelpers
    {
        /// <summary>
        /// Generates a string with a serial number.
        /// </summary>
        /// <param name="inputString">input String.</param>
        /// <returns>string.</returns>
        public static string GenerateStringWithSerialNumber(string inputString)
        {
            string pattern = @"\(\d+\)$";
            Regex r = new(pattern, RegexOptions.IgnoreCase);
            Match m = r.Match(inputString);

            if (!m.Success)
            {
                return $"{inputString}(1)";
            }

            string val = m.Value.Replace("(", string.Empty, StringComparison.Ordinal).Replace(")", string.Empty, StringComparison.Ordinal);
            int newVal = int.Parse(val, CultureInfo.InvariantCulture) + 1;
            return Regex.Replace(inputString, pattern, $"({newVal})");
        }
    }
}
