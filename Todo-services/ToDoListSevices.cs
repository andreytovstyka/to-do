﻿using Todo_data.Interfaces;
using Todo_domain_entities;
using Todo_services.Interfaces;

namespace Todo_services
{
    /// <summary>
    /// Manage of To-Do lists.
    /// </summary>
    public class ToDoListSevices : IToDoListSevices
    {
        private readonly IToDoListRepository listRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListSevices"/> class.
        /// </summary>
        /// <param name="listRepository">list.</param>
        /// <exception cref="ArgumentNullException">if listRepository is null.</exception>
        public ToDoListSevices(IToDoListRepository listRepository)
        {
            this.listRepository = listRepository ?? throw new ArgumentNullException(nameof(listRepository));
        }

        /// <inheritdoc />
        public async Task<bool> IsToDoListExistsAsync(long idToDoList)
        {
            return (await this.listRepository.GetAllAsync()).Any(x => x.Id == idToDoList);
        }

        /// <inheritdoc />
        public async Task<long> AddListAsync(ToDoList toDoList)
        {
            Helpers.IfNullException(toDoList, "list");

            await this.GetNewTitleAsync(toDoList, toDoList.Id);

            var newId = await this.listRepository.AddAsync(toDoList);
            return newId;
        }

        /// <inheritdoc />
        public async Task<IQueryable<ToDoList>> GetAllListsAsync() => await this.listRepository.GetAllAsync();

        /// <inheritdoc />
        public async Task UpdateListAsync(ToDoList toDoList)
        {
            Helpers.IfNullException(toDoList, "list");

            await this.GetNewTitleAsync(toDoList, toDoList.Id);

            await this.listRepository.UpdateAsync(toDoList);
        }

        /// <inheritdoc />
        public async Task<long> CopyListAsync(long id)
        {
            long newId = -1;
            ToDoList? toDoList = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == id);

            if (toDoList != null && toDoList.Clone() is ToDoList newToDoList)
            {
                await this.GetNewTitleAsync(newToDoList, null);
                newId = await this.listRepository.AddAsync(newToDoList);
            }

            return newId;
        }

        /// <inheritdoc />
        public async Task ChangeVisibilityAsync(long id)
        {
            ToDoList? toDoList = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == id);

            if (toDoList != null)
            {
                toDoList.Hide = !toDoList.Hide;
                await this.UpdateListAsync(toDoList);
            }
        }

        /// <inheritdoc />
        public async Task DeleteListAsync(long id)
        {
            ToDoList? toDoList = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == id);

            if (toDoList != null)
            {
                await this.listRepository.DeleteAsync(toDoList);
            }
        }

        private async Task GetNewTitleAsync(ToDoList toDoList, long? id)
        {
            var lenghtString = Todo_domain_entities.Helpers.GetMinAndMaxValueOfStringLengthAttribute<ToDoList>("Title");

            string newTitle = await this.GenerateTitleAsync(toDoList.Title, id, toDoList.UserId);

            try
            {
                toDoList.Title = newTitle;
            }
            catch (ArgumentException)
            {
                toDoList.Title = newTitle.Substring(0, lenghtString.maxLenght);
            }
        }

        private async Task<string> GenerateTitleAsync(string title, long? id, string userId)
        {
            bool isTitleExists = true;

            if (id != null)
            {
                isTitleExists = (await this.listRepository.GetAllAsync()).Any(x => x.Title == title && x.Id != id && x.UserId == userId);
            }
            else
            {
                isTitleExists = (await this.listRepository.GetAllAsync()).Any(x => x.Title == title && x.UserId == userId);
            }

            if (!isTitleExists)
            {
                return title;
            }

            return await this.GenerateTitleAsync(ServicesHelpers.GenerateStringWithSerialNumber(title), id, userId);
        }
    }
}