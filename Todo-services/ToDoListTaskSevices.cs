﻿using Todo_data.Interfaces;
using Todo_domain_entities;
using Todo_services.Interfaces;

namespace Todo_services
{
    /// <summary>
    /// Manage of To-Do tasks.
    /// </summary>
    public class ToDoListTaskSevices : IToDoListTaskSevices
    {
        private readonly IToDoListTaskRepository tasksRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListTaskSevices"/> class.
        /// </summary>
        /// <param name="taskRepository">list.</param>
        /// <exception cref="ArgumentNullException">if taskRepository is null.</exception>
        public ToDoListTaskSevices(IToDoListTaskRepository taskRepository)
        {
            this.tasksRepository = taskRepository ?? throw new ArgumentNullException(nameof(taskRepository));
        }

        /// <inheritdoc />
        public async Task<bool> IsTaskExistsAsync(long idTask)
        {
            return (await this.tasksRepository.GetAllAsync()).Any(x => x.Id == idTask);
        }

        /// <inheritdoc />
        public async Task AddTaskAsync(ToDoListTask toDoListTask)
        {
            Helpers.IfNullException(toDoListTask, "Task");

            await this.GetNewTitleAsync(toDoListTask);

            await this.tasksRepository.AddAsync(toDoListTask);
        }

        /// <inheritdoc />
        public async Task<IQueryable<ToDoListTask>> GetAllTasksAsync() => await this.tasksRepository.GetAllAsync();

        /// <inheritdoc />
        public async Task UpdateTaskAsync(ToDoListTask toDoListTask)
        {
            Helpers.IfNullException(toDoListTask, "Task");

            await this.GetNewTitleAsync(toDoListTask);

            await this.tasksRepository.UpdateAsync(toDoListTask);
        }

        /// <inheritdoc />
        public async Task ChangeStatusAsync(long idTask, Status newStatus)
        {
            ToDoListTask? task = (await this.tasksRepository.GetAllAsync()).FirstOrDefault(x => x.Id == idTask);
            if (task != null)
            {
                task.TaskStatus = newStatus;

                await this.UpdateTaskAsync(task);
            }
        }

        /// <inheritdoc />
        public async Task SetReminderAsync(long idTask, DateTime remind)
        {
            ToDoListTask? task = (await this.tasksRepository.GetAllAsync()).FirstOrDefault(x => x.Id == idTask);
            if (task != null)
            {
                task.Remind = remind;

                await this.UpdateTaskAsync(task);
            }
        }

        /// <inheritdoc />
        public async Task DeleteTaskAsync(long idTask)
        {
            ToDoListTask? task = (await this.tasksRepository.GetAllAsync()).FirstOrDefault(x => x.Id == idTask);

            if (task != null)
            {
                await this.tasksRepository.DeleteAsync(task);
            }
        }

        private async Task GetNewTitleAsync(ToDoListTask toDoListTask)
        {
            var lenghtString = Todo_domain_entities.Helpers.GetMinAndMaxValueOfStringLengthAttribute<ToDoListTask>("Title");

            string newTitle = await this.GenerateTitleAsync(toDoListTask.Title, toDoListTask.ToDoListId, toDoListTask.Id);

            try
            {
                toDoListTask.Title = newTitle;
            }
            catch (ArgumentException)
            {
                toDoListTask.Title = newTitle[..lenghtString.maxLenght];
            }
        }

        private async Task<string> GenerateTitleAsync(string title, long listId, long taskId)
        {
            bool isTitleExists = (await this.tasksRepository.GetAllAsync()).Any(x => x.Title == title && x.ToDoListId == listId && x.Id != taskId);

            if (!isTitleExists)
            {
                return title;
            }

            return await this.GenerateTitleAsync(ServicesHelpers.GenerateStringWithSerialNumber(title), listId, taskId);
        }
    }
}