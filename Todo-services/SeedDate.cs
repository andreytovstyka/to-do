﻿namespace Todo_services
{
    using Todo_data.Interfaces;
    using Todo_domain_entities;

    /// <summary>
    /// Seed data class.
    /// </summary>
    public static class SeedDate
    {
        /// <summary>
        /// Seed data.
        /// </summary>
        /// <param name="lists">lists repository.</param>
        /// <param name="tasks">tasks repository.</param>
        /// <param name="userId">user Id.</param>
        /// <returns>representing the asynchronous operation.</returns>
        public static async Task SeedAsync(IToDoListRepository lists, IToDoListTaskRepository tasks, string userId)
        {
            Helpers.IfNullException(lists, "ToDoListRepository");
            Helpers.IfNullException(tasks, "ToDoListTaskRepository");

            long id = await lists.AddAsync(new ToDoList(title: "Home", description: "home & family", userId: userId));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, title: "Budget planning", description: "next month", dueDate: DateTime.Now.Date, important: true, urgent: true, status: Status.Created));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, "Buy a climbing frame for a cat", "with a place to sleep", DateTime.Now.AddDays(2).Date, true, urgent: false, Status.Created));

            id = await lists.AddAsync(new ToDoList(title: "Job", description: "job & career", userId: userId));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, title: "Do To-Do List application.", description: "EPAM training", dueDate: DateTime.Now.AddDays(5).Date, important: true, urgent: true, status: Status.Created));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, title: "Do First EF Core application.", description: "EPAM training", dueDate: DateTime.Now.Date, important: true, urgent: true, status: Status.Completed, remind: DateTime.Now.AddDays(-1)));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, title: "Do ADO.NET application.", description: "EPAM training", dueDate: DateTime.Now.AddDays(5).Date, important: true, urgent: true, status: Status.Created));

            id = await lists.AddAsync(new ToDoList(title: "Hobby", description: "favorite activities & health", userId: userId));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, title: "Run a marathon.", description: "everything is possible", dueDate: DateTime.Now.AddYears(1).Date, important: true, urgent: false, status: Status.Completed));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, title: "See the Grand Canyon.", description: "at first think of a plan", dueDate: DateTime.Now.AddYears(2).Date, important: true, urgent: false, status: Status.Created));
            await tasks.AddAsync(new ToDoListTask(toDoListId: id, title: "Buy a fishing rod.", description: "2 is better", dueDate: DateTime.Now.AddDays(20).Date, important: false, urgent: false, status: Status.Created));
        }
    }
}
