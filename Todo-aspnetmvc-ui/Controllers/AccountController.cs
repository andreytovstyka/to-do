﻿using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Todo_aspnetmvc_ui.Models.ViewModels;

namespace Todo_aspnetmvc_ui.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<ListController> logger;

        public AccountController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IServiceProvider serviceProvider,
            ILogger<ListController> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public IActionResult Login(string? returnUrl = null)
        {
            return this.View(new LoginViewModel()
            {
                ReturnUrl = returnUrl ?? "/",
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel login)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View();
            }

            var result = await this.signInManager.PasswordSignInAsync(login.EmailAddress, login.Password, login.RememberMe, false);

            if (!result.Succeeded)
            {
                this.ModelState.AddModelError(string.Empty, "Login error!");

                this.logger.LogInformation($"Failed login attempt for user {login.EmailAddress} at {DateTime.UtcNow}");

                return this.View();
            }

            this.logger.LogInformation($"User {login.EmailAddress} login at {DateTime.UtcNow}");

            if (string.IsNullOrWhiteSpace(login.ReturnUrl))
            {
                return this.Redirect("/");
            }

            return this.Redirect(login.ReturnUrl);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            this.logger.LogInformation($"User {this.User.FindFirstValue(ClaimTypes.NameIdentifier)} logout at {DateTime.UtcNow}");

            await this.signInManager.SignOutAsync();
            this.Response.Cookies.Delete(".AspNetCore.Session");
            this.Response.Cookies.Delete("ASP.NET_SessionId");

            string returnUrl = "/";
            if (!string.IsNullOrEmpty(this.Request.Headers["Referer"].ToString()))
            {
                returnUrl = this.Request.Headers["Referer"].ToString().Replace(this.Request.Headers["Origin"].ToString(), string.Empty, StringComparison.Ordinal);
            }

            return this.RedirectToAction("Login", "Account", new { ReturnUrl = returnUrl });
        }

        public IActionResult Register()
        {
            return this.View(new RegisterViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registration)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registration);
            }

            var newUser = new IdentityUser
            {
                Email = registration.EmailAddress,
                UserName = registration.EmailAddress,
            };

            var result = await this.userManager.CreateAsync(newUser, registration.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors.Select(x => x.Description))
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }

                return this.View();
            }

            var user = await this.userManager.FindByNameAsync(newUser.UserName);

            if (user != null)
            {
                await Helpers.Helpers.SeedAsync(this.serviceProvider, user.Id);

                this.logger.LogInformation($"User {user.Id} register at {DateTime.UtcNow}");
            }

            return this.RedirectToAction("Login");
        }
    }
}
