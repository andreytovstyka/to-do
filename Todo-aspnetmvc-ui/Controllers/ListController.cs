﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Todo_aspnetmvc_ui.Models.ViewModels;
using Todo_domain_entities;
using Todo_services.Interfaces;

namespace Todo_aspnetmvc_ui.Controllers
{
    /// <summary>
    /// List controller.
    /// </summary>
    [Authorize]
    public class ListController : Controller
    {
        private readonly IToDoListSevices listServices;
        private readonly ILogger<ListController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListController"/> class.
        /// </summary>
        /// <param name="listServices">list services.</param>
        /// <param name="logger">logger.</param>
        public ListController(IToDoListSevices listServices, ILogger<ListController> logger)
        {
            this.listServices = listServices;
            this.logger = logger;
        }

        /// <summary>
        /// UI to create the ToDoList.
        /// </summary>
        /// <returns>The view to create the ToDoList.</returns>
        public IActionResult Create()
        {
            var listMod = new ListDataInputViewModel
            {
                TodoList = new ToDoList(),
                ReturnUrl = this.GetRefer(),
            };

            this.logger.LogInformation($"The form to create a new todo list was shown to user {this.UserId()} at {DateTime.Now}");

            return this.View(listMod);
        }

        /// <summary>
        /// create new list.
        /// </summary>
        /// <param name="list">new ToDoList.</param>
        /// <returns>tasks list.</returns>
        [HttpPost]
        public async Task<IActionResult> Create(ToDoList list)
        {
            long newId = 0;

            if (!this.ModelState.IsValid)
            {
                return this.View(list);
            }

            list.UserId = this.UserId();

            try
            {
                newId = await this.listServices.AddListAsync(list);
                this.logger.LogInformation($"New list <{list.Title}> added {DateTime.Now} by user {list.UserId}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to create a new todo list. Passed values: UserId <{list.UserId}>, Title <{list.Title}>, Description <{list.Description}>, Hide <{list.Hide}>");
                return this.Redirect("/Error.html");
            }

            return this.RedirectToAction("Tasks", "Task", new { id = newId });
        }

        /// <summary>
        /// UI to edit the ToDoList.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <returns>The view to edit the ToDoList.</returns>
        public async Task<ActionResult> Edit(long id)
        {
            await this.IfNotCurrentUserExeptionAsync(idList: id);

            var list = (await this.listServices.GetAllListsAsync()).FirstOrDefault(x => x.Id == id);

            var listMod = new ListDataInputViewModel
            {
                TodoList = list,
                ReturnUrl = this.GetRefer(),
            };

            this.logger.LogInformation($"The form to edit todo list {id} was shown to user {this.UserId()} at {DateTime.Now}");

            return this.View(listMod);
        }

        /// <summary>
        /// Save list.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <param name="list">edited list.</param>
        /// <param name="returnUrl">return Url.</param>
        /// <returns>All tasks in list.</returns>
        [HttpPost]
        public async Task<ActionResult> Edit(long id, ToDoList list, string returnUrl)
        {
            await this.IfNotCurrentUserExeptionAsync(idList: id);

            if (!this.ModelState.IsValid)
            {
                return this.View(list);
            }

            try
            {
                await this.listServices.UpdateListAsync(list);
                this.logger.LogInformation($"List {id} edited at {DateTime.Now} by user {this.UserId()}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to edit todo list {id}. Passed values: UserId <{list.UserId}>, Title <{list.Title}>, Description <{list.Description}>, Hide <{list.Hide}>");
                return this.Redirect("/Error.html");
            }

            return this.Redirect(returnUrl);
        }

        /// <summary>
        /// Hide or show a list in an application's list view.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <returns>lists view.</returns>
        public async Task<ActionResult> ChangeVisibility(long id)
        {
            await this.IfNotCurrentUserExeptionAsync(idList: id);

            try
            {
                await this.listServices.ChangeVisibilityAsync(id);

                this.logger.LogInformation($"Visibility of list {id} changed at {DateTime.Now} by user {this.UserId()}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to change visibility of todo list {id} by user {this.UserId()}.");
                return this.Redirect("/Error.html");
            }

            return this.Redirect(this.GetRefer());
        }

        /// <summary>
        /// Create a copy of the list.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <returns>All list.</returns>
        public async Task<ActionResult> Copy(long id)
        {
            await this.IfNotCurrentUserExeptionAsync(idList: id);

            long newId = 0;

            try
            {
                newId = await this.listServices.CopyListAsync(id);
                this.logger.LogInformation($"A copy of the todo list {id} has been created. New to-do list ID: {newId}. Done on {DateTime.Now} by user {this.UserId()}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to create a copy of todo list {id}.");
                return this.Redirect("/Error.html");
            }

            return this.RedirectToAction("Tasks", "Task", new { id = newId });
        }

        /// <summary>
        /// Confirmation before deletion.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <returns>Confirmation view.</returns>
        [HttpGet]
        public async Task<ActionResult> Delete(long id)
        {
            await this.IfNotCurrentUserExeptionAsync(idList: id);
            ToDoList? list = (await this.listServices.GetAllListsAsync()).FirstOrDefault(x => x.Id == id);

            var listMod = new ListDataInputViewModel
            {
                TodoList = list,
                ReturnUrl = this.GetRefer(),
            };

            this.logger.LogInformation($"The form to delete todo list {id} was shown to user {this.UserId()} at {DateTime.Now}");

            return this.View(listMod);
        }

        /// <summary>
        /// Delete list.
        /// </summary>
        /// <param name="toDoList">list.</param>
        /// <returns>All tasks in list.</returns>
        [HttpPost]
        public async Task<ActionResult> Delete(ToDoList toDoList)
        {
            var idList = toDoList.Id;

            await this.IfNotCurrentUserExeptionAsync(idList);

            try
            {
                await this.listServices.DeleteListAsync(toDoList.Id);
                this.logger.LogInformation($"Todo list {idList} has been deleted at {DateTime.Now} by user {this.UserId()}.");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to delete todo list {idList}.");
                return this.Redirect("/Error.html");
            }

            return this.RedirectToAction("Index", "Home");
        }

        private string UserId()
        {
            return this.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        private async Task IfNotCurrentUserExeptionAsync(long idList)
        {
            var userId = this.UserId();

            if (!(await this.listServices.GetAllListsAsync()).Any(x => x.Id == idList && x.UserId == userId))
            {
                this.logger.LogWarning($"User {this.UserId()} tryed to get access to strange todo list {idList}.");

                throw new InvalidOperationException("not a valid user!!!");
            }
        }

        private string GetRefer()
        {
            var result = this.Request.Headers["Referer"].ToString();
            if (string.IsNullOrEmpty(result))
            {
                return "/";
            }

            return result;
        }
    }
}