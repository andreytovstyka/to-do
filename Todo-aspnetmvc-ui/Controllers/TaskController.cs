﻿using System.Globalization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Todo_aspnetmvc_ui.Models.ViewModels;
using Todo_data.Migrations;
using Todo_domain_entities;
using Todo_services.Interfaces;

namespace Todo_aspnetmvc_ui.Controllers
{
    /// <summary>
    /// Task Controller.
    /// </summary>
    [Authorize]
    public class TaskController : Controller
    {
        private readonly IToDoListTaskSevices taskServices;
        private readonly IToDoListSevices listServices;
        private readonly ILogger<TaskController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskController"/> class.
        /// </summary>
        /// <param name="taskServices">task Services.</param>
        /// <param name="listServices">list Services.</param>
        /// <param name="logger">logger.</param>
        public TaskController(IToDoListTaskSevices taskServices, IToDoListSevices listServices, ILogger<TaskController> logger)
        {
            this.listServices = listServices;
            this.taskServices = taskServices;
            this.logger = logger;
        }

        /// <summary>
        /// Index.
        /// </summary>
        /// <param name="id">list id.</param>
        /// <returns>All tasks in list.</returns>
        public async Task<IActionResult> Tasks(long id)
        {
            await this.IfNotCurrentUserExeptionAsync(idList: id);

            var list = (await this.listServices.GetAllListsAsync()).FirstOrDefault(x => x.Id == id && x.UserId == this.UserId());

            var tasks = new TasksViewModel
            {
                TodoTasks = list?.ToDoListTasks ?? Enumerable.Empty<ToDoListTask>(),
                CurrentToDoList = id,
            };

            string listName = list?.Title ?? string.Empty;

            this.ViewBag.listName = listName;
            this.ViewBag.back = listName;

            this.logger.LogInformation($"{id} to-do list task list shown to user {this.UserId()} on {DateTime.Now}");

            return this.View(tasks);
        }

        /// <summary>
        /// Today, planned and overdue tasks.
        /// </summary>
        /// <param name="filter">Filter (today, planned or overdue).</param>
        /// <returns>filtered tasks.</returns>
        public async Task<IActionResult> Filter(string filter)
        {
            IEnumerable<ToDoListTask>? tasks = Enumerable.Empty<ToDoListTask>();

            if (filter.ToLower(CultureInfo.InvariantCulture) == "today")
            {
                tasks = from list in await this.listServices.GetAllListsAsync()
                        where list.UserId == this.UserId()
                        join task in await this.taskServices.GetAllTasksAsync()
                        on list.Id equals task.ToDoListId
                        where task.TaskStatus != Status.Completed &&
                        task.DueDate == DateTime.Now.Date
                        select task;
            }
            else if (filter.ToLower(CultureInfo.InvariantCulture) == "planned")
            {
                tasks = from list in await this.listServices.GetAllListsAsync()
                        where list.UserId == this.UserId()
                        join task in await this.taskServices.GetAllTasksAsync()
                        on list.Id equals task.ToDoListId
                        where task.TaskStatus != Status.Completed &&
                        task.DueDate > DateTime.Now.Date
                        select task;
            }
            else if (filter.ToLower(CultureInfo.InvariantCulture) == "overdue")
            {
                tasks = from list in await this.listServices.GetAllListsAsync()
                        where list.UserId == this.UserId()
                        join task in await this.taskServices.GetAllTasksAsync()
                        on list.Id equals task.ToDoListId
                        where task.TaskStatus != Status.Completed &&
                        task.DueDate < DateTime.Now.Date
                        select task;
            }

            var tasksMod = new TasksViewModel
            {
                TodoTasks = tasks,
            };

            this.ViewBag.listName = filter;

            this.logger.LogInformation($"{filter} tasks list shown to user {this.UserId()} on {DateTime.Now}");

            return this.View("Tasks", tasksMod);
        }

        /// <summary>
        /// Reminders tasks.
        /// </summary>
        /// <returns>reminders tasks.</returns>
        public async Task<IActionResult> Reminders()
        {
            IEnumerable<ToDoListTask>? tasks = Enumerable.Empty<ToDoListTask>();

            var userId = this.UserId() ?? string.Empty;

            tasks = from list in await this.listServices.GetAllListsAsync()
                    where list.UserId == userId
                    join task in await this.taskServices.GetAllTasksAsync()
                    on list.Id equals task.ToDoListId
                    where task.Remind < DateTime.UtcNow && task.Remind > DateTime.Now.AddYears(-100)
                    select task;

            var tasksMod = new TasksViewModel
            {
                TodoTasks = tasks,
            };

            this.ViewBag.listName = "Reminder";

            this.logger.LogInformation($"Tasks with reminders were shown to the user {this.UserId()} on {DateTime.Now}");

            return this.View("Tasks", tasksMod);
        }

        /// <summary>
        /// UI to create the task.
        /// </summary>
        /// <param name="id">id task.</param>
        /// <returns>The view to create the task.</returns>
        public ActionResult Create(long id)
        {
            var task = new ToDoListTask();
            task.ToDoListId = id;

            var taskMod = new TaskDataInputViewModel
            {
                ToDoTask = task,
                ReturnUrl = this.GetRefer(),
            };

            this.logger.LogInformation($"The form to create a new task was shown to user {this.UserId()} at {DateTime.Now}");

            return this.View(taskMod);
        }

        /// <summary>
        /// Save new task.
        /// </summary>
        /// <param name="id">id list.</param>
        /// <param name="task">created task.</param>
        /// <param name="returnUrl">return Url.</param>
        /// <returns>List of tasks.</returns>
        [HttpPost]
        public async Task<ActionResult> Create(long id, ToDoListTask task, string returnUrl)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(new TaskDataInputViewModel
                {
                    ToDoTask = task,
                    ReturnUrl = returnUrl,
                });
            }

            await this.IfNotCurrentUserExeptionAsync(idList: id);

            task.ToDoListId = id;

            try
            {
                await this.taskServices.AddTaskAsync(task);
                this.logger.LogInformation($"New task <{task.Title}> added to list {id} {DateTime.Now} by user {this.UserId()}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to create a new task in {id} list. Passed values: Title <{task.Title}>, Description <{task.Description}>, DueDate <{task.DueDate}>, Remind <{task.Remind}>, Important <{task.Important}>, Urgent <{task.Urgent}>");
            }

            return this.RedirectToAction("Tasks", "Task", new { id });
        }

        /// <summary>
        /// UI to edit the task.
        /// </summary>
        /// <param name="id">id task.</param>
        /// <returns>The view to edit the task.</returns>
        public async Task<ActionResult> Edit(long id)
        {
            ToDoListTask? task = (await this.taskServices.GetAllTasksAsync()).FirstOrDefault(t => t.Id == id);

            if (task != null)
            {
                await this.IfNotCurrentUserExeptionAsync(task.ToDoListId);
            }

            var taskMod = new TaskDataInputViewModel
            {
                ToDoTask = task,
                ReturnUrl = this.GetRefer(),
            };

            this.logger.LogInformation($"The form to edit task {id} was shown to user {this.UserId()} at {DateTime.Now}");

            return this.View(taskMod);
        }

        /// <summary>
        /// Save edited task.
        /// </summary>
        /// <param name="task">edited task.</param>
        /// <param name="returnUrl">returnUrl.</param>
        /// <returns>List of tasks.</returns>
        [HttpPost]
        public async Task<ActionResult> Edit(ToDoListTask task, string returnUrl)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(new TaskDataInputViewModel
                {
                    ToDoTask = task,
                    ReturnUrl = returnUrl,
                });
            }

            await this.IfNotCurrentUserExeptionAsync(task.ToDoListId);

            try
            {
                await this.taskServices.UpdateTaskAsync(task);
                this.logger.LogInformation($"Task {task.Id} edited {DateTime.Now} by user {this.UserId()}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to edit task {task.Id}. Passed values: Title <{task.Title}>, Description <{task.Description}>, DueDate <{task.DueDate}>, Remind <{task.Remind}>, Important <{task.Important}>, Urgent <{task.Urgent}>");
            }

            return this.Redirect(returnUrl);
        }

        /// <summary>
        /// Save new status.
        /// </summary>
        /// <param name="idTask">id Task.</param>
        /// <param name="idList">id List.</param>
        /// <param name="newStatus">new Status.</param>
        /// <returns>updated page.</returns>
        [HttpPost]
        public async Task<ActionResult> ChangeStatus(long idTask, long idList, string newStatus)
        {
            await this.IfNotCurrentUserExeptionAsync(idList);

            var numStatus = byte.Parse(newStatus, CultureInfo.InvariantCulture);

            try
            {
                await this.taskServices.ChangeStatusAsync(idTask, (Status)numStatus);
                this.logger.LogInformation($"Status of task {idTask} changed {DateTime.Now} by user {this.UserId()}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to changing task status. Task {idTask}. {DateTime.UtcNow} by user {this.UserId()}");
            }

            return this.Redirect(this.GetRefer());
        }

        /// <summary>
        /// Confirmation before task deletion.
        /// </summary>
        /// <param name="id">task id.</param>
        /// <returns>Confirmation view.</returns>
        [HttpGet]
        public async Task<ActionResult> Delete(long id)
        {
            ToDoListTask? task = (await this.taskServices.GetAllTasksAsync()).FirstOrDefault(t => t.Id == id);

            if (task != null)
            {
                await this.IfNotCurrentUserExeptionAsync(task.ToDoListId);
            }

            var taskMod = new TaskDataInputViewModel
            {
                ToDoTask = task,
                ReturnUrl = this.GetRefer(),
            };

            this.logger.LogInformation($"The form to delete task {id} was shown to user {this.UserId()} at {DateTime.Now}");

            return this.View("Delete", taskMod);
        }

        /// <summary>
        /// Delete task.
        /// </summary>
        /// <param name="task">task.</param>
        /// <param name="returnUrl">returnUrl.</param>
        /// <returns>All tasks in list.</returns>
        [HttpPost]
        public async Task<ActionResult> Delete(ToDoListTask task, string returnUrl)
        {
            if (task != null)
            {
                await this.IfNotCurrentUserExeptionAsync(task.ToDoListId);

                var idTask = task.Id;
                try
                {
                    await this.taskServices.DeleteTaskAsync(idTask);
                    this.logger.LogInformation($"Task {idTask} deleted {DateTime.Now} by user {this.UserId()}");
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to deleting task {idTask} on {DateTime.UtcNow} by user {this.UserId()}");
                }
            }

            return this.Redirect(returnUrl);
        }

        /// <summary>
        /// Set reminder.
        /// </summary>
        /// <param name="idTask">id Task.</param>
        /// <param name="idList">id List.</param>
        /// <param name="remind">remind DateTime.</param>
        /// <returns>updated page.</returns>
        [HttpPost]
        public async Task<ActionResult> SetReminder(long idTask, long idList, DateTime remind)
        {
            await this.IfNotCurrentUserExeptionAsync(idList);

            try
            {
                await this.taskServices.SetReminderAsync(idTask, remind);
                this.logger.LogInformation($"The reminder for task {idTask} was set on {DateTime.Now} by user {this.UserId()}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, $"{ex.Message}. An error occurred while trying to setting reminder for task {idTask} on {DateTime.UtcNow} by user {this.UserId()}");
            }

            return this.Redirect(this.GetRefer());
        }

        private string UserId()
        {
            return this.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        private async Task IfNotCurrentUserExeptionAsync(long idList)
        {
            var userId = this.UserId();

            if (!(await this.listServices.GetAllListsAsync()).Any(x => x.Id == idList && x.UserId == userId))
            {
                throw new InvalidOperationException("not a valid user!!!");
            }
        }

        private string GetRefer()
        {
            var result = this.Request.Headers["Referer"].ToString();
            if (string.IsNullOrEmpty(result))
            {
                return "/";
            }

            return result;
        }
    }
}
