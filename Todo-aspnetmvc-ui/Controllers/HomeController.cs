﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Todo_aspnetmvc_ui.Controllers
{
    /// <summary>
    /// Home controller.
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="logger">logger.</param>
        public HomeController(ILogger<HomeController> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Main page.
        /// </summary>
        /// <returns>Index page.</returns>
        public IActionResult Index()
        {
            this.logger.LogInformation($"The list of todo lists was shown to user {this.UserId()} on {DateTime.UtcNow}");

            return this.View();
        }

        /// <summary>
        /// Сhoice of view for the error.
        /// </summary>
        /// <param name="statusCode">status Code.</param>
        /// <returns>view.</returns>
        public IActionResult Error(int statusCode)
        {
            this.ViewBag.fileName = "commonError.png";

            switch (statusCode)
            {
                case 404:
                    this.ViewBag.message = "Page not found.";
                    this.ViewBag.fileName = "pageNotFoundError.png";
                    break;
                case int n when n < 500 && n >= 400:
                    this.ViewBag.message = "Client Error.";
                    break;
                case int n when n >= 500 && n < 600:
                    this.ViewBag.message = "Server Error.";
                    break;
                default:
                    this.ViewBag.message = "Unknown Error.";
                    break;
            }

            this.logger.LogError($"{this.ViewBag.message} Query string {this.Request.Headers["Referer"]} of user {this.UserId()} on {DateTime.UtcNow}");

            return this.View();
        }

        private string UserId()
        {
            return this.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
    }
}