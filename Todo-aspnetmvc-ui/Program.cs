using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Todo_data;
using Todo_data.Interfaces;
using Todo_data.RepositorySql;
using Todo_services;
using Todo_services.Interfaces;

namespace Todo_aspnetmvc_ui
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddControllersWithViews();

            builder.Services.AddDbContext<ToDoContext>(options =>
            {
                options.UseSqlServer(builder.Configuration["ConnectionStrings:todoDB"]);
            });

            builder.Services.AddDbContext<ToDoIdentityDbContext>(options => options.UseSqlServer(builder.Configuration["ConnectionStrings:IdentityDB"]));
            builder.Services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<ToDoIdentityDbContext>();

            builder.Services.AddScoped<IToDoListRepository, ToDoListRepositorySql>();
            builder.Services.AddScoped<IToDoListTaskRepository, ToDoListTaskRepositorySql>();
            builder.Services.AddScoped<IToDoListSevices, ToDoListSevices>();
            builder.Services.AddScoped<IToDoListTaskSevices, ToDoListTaskSevices>();

            builder.Services.AddDistributedMemoryCache();
            builder.Services.AddSession();

            var app = builder.Build();

            var loggerFactory = app.Services.GetService<ILoggerFactory>();
            loggerFactory.AddFile(builder.Configuration["Logging:LogFilePath"].ToString());

            if (app.Environment.IsProduction())
            {
                app.UseExceptionHandler("/Error.html");
                app.UseStatusCodePagesWithReExecute("/Home/Error", "?statusCode={0}");
            }
            else
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            Helpers.Helpers.CheckMigration(app);

            app.Run();
        }
    }
}