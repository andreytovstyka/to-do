﻿function show(el) {
    var divide = document.getElementById(el);
    if (divide.style.display === "none") {
        divide.style.display = "block";
    } else {
        divide.style.display = "none";
    }
}