﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Todo_services.Interfaces;

namespace Todo_aspnetmvc_ui.ViewComponents
{
    [ViewComponent]
    public class TodoListsViewComponent : ViewComponent
    {
        private readonly IToDoListSevices listServices;
        private readonly UserManager<IdentityUser> userManager;

        public TodoListsViewComponent(UserManager<IdentityUser> userManager, IToDoListSevices listServices)
        {
            this.listServices = listServices;
            this.userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var userId = this.userManager.GetUserId(this.UserClaimsPrincipal);

            var lists = (await this.listServices.GetAllListsAsync()).Where(x => x.UserId == userId);

            return this.View(lists);
        }
    }
}
