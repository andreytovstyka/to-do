﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Todo_domain_entities;
using Todo_services.Interfaces;

namespace Todo_aspnetmvc_ui.ViewComponents
{
    [ViewComponent]
    public class RemainderViewComponent : ViewComponent
    {
        private readonly IToDoListSevices listServices;
        private readonly IToDoListTaskSevices taskServices;
        private readonly UserManager<IdentityUser> userManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="RemainderViewComponent"/> class.
        /// </summary>
        /// <param name="userManager">userManager.</param>
        /// <param name="listServices">listServices.</param>
        /// <param name="taskServices">taskServices.</param>
        public RemainderViewComponent(UserManager<IdentityUser> userManager, IToDoListSevices listServices, IToDoListTaskSevices taskServices)
        {
            this.listServices = listServices;
            this.taskServices = taskServices;
            this.userManager = userManager;
        }

        /// <summary>
        /// Return number of reminders to the ViewBag.
        /// </summary>
        /// <returns>Number of reminders.</returns>
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var userId = this.userManager.GetUserId(this.UserClaimsPrincipal);

            var tasks = from list in await this.listServices.GetAllListsAsync()
                    where list.UserId == userId
                    join task in await this.taskServices.GetAllTasksAsync()
                    on list.Id equals task.ToDoListId
                    where task.Remind < DateTime.UtcNow && task.Remind > DateTime.Now.AddYears(-100)
                    select task.Id;

            this.ViewBag.reminders = tasks.Count();
            return this.View();
        }
    }
}
