﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Todo_data;
using Todo_data.Interfaces;

namespace Todo_aspnetmvc_ui.Helpers
{
    public static class Helpers
    {
        /// <summary>
        /// Get display name from enum field.
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="value">value.</param>
        /// <returns>Display name.</returns>
        /// <exception cref="ArgumentNullException">if value is null.</exception>
        public static string GetDisplayNameFromEnumField<T>(T value)
            where T : Enum
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            string propName = value.ToString() ?? string.Empty;

            FieldInfo? fieldInfo = value.GetType().GetField(propName, BindingFlags.Static | BindingFlags.Public);

            if (fieldInfo == null)
            {
                return propName;
            }

            CustomAttributeData? attribute = fieldInfo.CustomAttributes.FirstOrDefault(x => x.AttributeType.Name == "DisplayAttribute");

            if (attribute == null)
            {
                return propName;
            }

            var namedArgumentObj = attribute.NamedArguments.FirstOrDefault().TypedValue.Value;

            if (namedArgumentObj == null)
            {
                return propName;
            }

            return namedArgumentObj.ToString() ?? propName;
        }

        /// <summary>
        /// String number.
        /// </summary>
        /// <param name="number">number.</param>
        /// <param name="singular">singular string.</param>
        /// <param name="plural">plural string.</param>
        /// <returns>string number.</returns>
        public static string GetStringNumberOf(int number, string singular, string plural)
        {
            string str = $"no {plural}";

            if (number == 1)
            {
                str = $"1 {singular}";
            }
            else
            {
                str = $"{number} {plural}";
            }

            return str;
        }

        /// <summary>
        /// Seed data.
        /// </summary>
        /// <param name="app">IApplicationBuilder.</param>
        public static void CheckMigration(IApplicationBuilder app)
        {
            ToDoContext context = app.ApplicationServices
            .CreateScope().ServiceProvider.GetRequiredService<ToDoContext>();

            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            ToDoIdentityDbContext contexIdentityt = app.ApplicationServices
            .CreateScope().ServiceProvider.GetRequiredService<ToDoIdentityDbContext>();

            if (contexIdentityt.Database.GetPendingMigrations().Any())
            {
                contexIdentityt.Database.Migrate();
            }
        }

        /// <summary>
        /// Loading test data.
        /// </summary>
        /// <param name="serviceProvider">service Provider.</param>
        /// <param name="userId">user Id.</param>
        /// <returns>representing the asynchronous operation.</returns>
        public static async Task SeedAsync(IServiceProvider serviceProvider, string userId)
        {
            try
            {
                IToDoListRepository listRepo = serviceProvider.GetRequiredService<IToDoListRepository>();

                IToDoListTaskRepository taskRepo = serviceProvider.GetRequiredService<IToDoListTaskRepository>();

                if (!string.IsNullOrEmpty(userId) && !await (await listRepo.GetAllAsync()).AnyAsync(x => x.UserId == userId))
                {
                    await Todo_services.SeedDate.SeedAsync(listRepo, taskRepo, userId);
                }
            }
            catch (InvalidOperationException)
            {
                return;
            }
        }
    }
}
