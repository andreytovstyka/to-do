﻿using System.ComponentModel.DataAnnotations;

namespace Todo_aspnetmvc_ui.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress,]
        [MaxLength(100)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; } = string.Empty;

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } = string.Empty;

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; } = "/";
    }
}
