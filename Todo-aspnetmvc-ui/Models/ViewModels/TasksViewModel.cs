﻿using System.Runtime.CompilerServices;
using Todo_domain_entities;

namespace Todo_aspnetmvc_ui.Models.ViewModels
{
    public class TasksViewModel
    {
        public IEnumerable<ToDoListTask> TodoTasks { get; set; } = Enumerable.Empty<ToDoListTask>();

        public long CurrentToDoList { get; set; } = -1;

        public string ReturnUrl { get; set; } = "/";
    }
}
