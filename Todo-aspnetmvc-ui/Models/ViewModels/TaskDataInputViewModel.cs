﻿using Todo_domain_entities;

namespace Todo_aspnetmvc_ui.Models.ViewModels
{
    public class TaskDataInputViewModel
    {
        public ToDoListTask? ToDoTask { get; set; }  

        public string ReturnUrl { get; set; } = "/";
    }
}
