﻿using System.Runtime.CompilerServices;
using Todo_domain_entities;

namespace Todo_aspnetmvc_ui.Models.ViewModels
{
    public class ListDataInputViewModel
    {
        public ToDoList? TodoList { get; set; }

        public string ReturnUrl { get; set; } = "/";
    }
}
