﻿namespace Todo_data.Interfaces
{
    using System;
    using Todo_domain_entities;

    /// <summary>
    /// ToDoListTask interface.
    /// </summary>
    public interface IToDoListTaskRepository
    {
        /// <summary>
        /// Gets all tasks.
        /// </summary>
        /// <returns>tasks.</returns>
        Task<IQueryable<ToDoListTask>> GetAllAsync();

        /// <summary>
        /// Add new task.
        /// </summary>
        /// <param name="task">task.</param>
        /// <exception cref="ArgumentNullException">if task is null.</exception>
        /// <returns>Task.</returns>
        Task AddAsync(ToDoListTask task);

        /// <summary>
        /// Update task by id.
        /// </summary>
        /// <param name="task">task.</param>
        /// <exception cref="ArgumentNullException">if task is null.</exception>
        /// <returns>Task.</returns>
        Task UpdateAsync(ToDoListTask task);

        /// <summary>
        /// Delete task.
        /// </summary>
        /// <param name="task">task.</param>
        /// <exception cref="ArgumentNullException">if task is null.</exception>
        /// <returns>Task.</returns>
        Task DeleteAsync(ToDoListTask task);
    }
}
