﻿namespace Todo_data.Interfaces
{
    using System;
    using Todo_domain_entities;

    /// <summary>
    /// ToDoList interface.
    /// </summary>
    public interface IToDoListRepository
    {
        /// <summary>
        /// Gets all ToDoList.
        /// </summary>
        /// <returns>All ToDoList.</returns>
        Task<IQueryable<ToDoList>> GetAllAsync();

        /// <summary>
        /// Add new ToDoList.
        /// </summary>
        /// <param name="list">To-Do list.</param>
        /// <exception cref="ArgumentNullException">if list is null.</exception>
        /// <returns>id new toDoList.</returns>
        Task<long> AddAsync(ToDoList list);

        /// <summary>
        /// Update ToDoList by id.
        /// </summary>
        /// <param name="list">To-Do list.</param>
        /// <returns>Task.</returns>
        Task UpdateAsync(ToDoList list);

        /// <summary>
        /// Delete ToDoList.
        /// </summary>
        /// <param name="list">To-Do list.</param>
        /// <exception cref="ArgumentNullException">if list is null.</exception>
        /// <returns>Task.</returns>
        Task DeleteAsync(ToDoList list);
    }
}
