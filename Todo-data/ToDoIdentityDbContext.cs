﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Todo_data
{
    public class ToDoIdentityDbContext : IdentityDbContext<IdentityUser>
    {
        public ToDoIdentityDbContext(DbContextOptions<ToDoIdentityDbContext> options)
            : base(options)
        {
        }
    }
}
