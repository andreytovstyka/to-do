﻿using System.Data;
using System.Data.SqlClient;
using Todo_data.Interfaces;
using Todo_domain_entities;

namespace Todo_data.RepositorySql
{
    /// <summary>
    /// To-Do list Sql repository.
    /// </summary>
    public class ToDoListTaskRepositoryADO : IToDoListTaskRepository
    {
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListTaskRepositoryADO"/> class.
        /// </summary>
        /// <param name="toDoContext">context.</param>
        /// <param name="connectionString">connection String.</param>
        public ToDoListTaskRepositoryADO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <inheritdoc />
        public async Task<IQueryable<ToDoListTask>> GetAllAsync()
        {
            List<ToDoListTask> tasks = new List<ToDoListTask>();
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string query = "SELECT ToDoListTasks.* FROM ToDoListTasks";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader rdr = await cmd.ExecuteReaderAsync();
                while (await rdr.ReadAsync())
                {
                    ToDoListTask task = new ToDoListTask()
                    {
                        DateCreation = (DateTime)rdr["DateCreation"],
                        Description = (string)rdr["Description"],
                        DueDate = (DateTime)rdr["DueDate"],
                        Remind = (rdr["Remind"] is not DBNull) ? (DateTime)rdr["Remind"] : null,
                        Id = (long)rdr["Id"],
                        Important = (bool)rdr["Important"],
                        Urgent = (bool)rdr["Urgent"],
                        Title = (string)rdr["Title"],
                        ToDoListId = (long)rdr["ToDoListId"],
                        TaskStatus = (Status)rdr["TaskStatus"],
                    };
                    tasks.Add(task);
                }

                rdr.Close();
            }

            return tasks.AsQueryable();
        }

        /// <inheritdoc />
        public async Task AddAsync(ToDoListTask task)
        {
            Helpers.IfNullException(task, "task");

            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string query = "INSERT INTO [dbo].[ToDoListTasks] ([TaskStatus],[DueDate],[Important],[Urgent],[ToDoListId],[Title],[Description],[DateCreation],[Remind]) VALUES (@TaskStatus, @DueDate, @Important, @Urgent, @ToDoListId, @Title, @Description, @DateCreation, @Remind)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.Add("@TaskStatus", SqlDbType.TinyInt).Value = (byte)task.TaskStatus;
                cmd.Parameters.Add("@DueDate", SqlDbType.DateTime2).Value = task.DueDate;
                cmd.Parameters.Add("@Important", SqlDbType.Bit).Value = task.Important;
                cmd.Parameters.Add("@Urgent", SqlDbType.Bit).Value = task.Urgent;
                cmd.Parameters.Add("@ToDoListId", SqlDbType.BigInt).Value = task.ToDoListId;
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = task.Title;
                cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = task.Description;
                cmd.Parameters.Add("@DateCreation", SqlDbType.DateTime2).Value = task.DateCreation;
                cmd.Parameters.Add("@Remind", SqlDbType.DateTime2).Value = (object?)task.Remind ?? DBNull.Value;
                con.Open();
                await cmd.ExecuteNonQueryAsync();
            }
        }

        /// <inheritdoc />
        public async Task UpdateAsync(ToDoListTask task)
        {
            Helpers.IfNullException(task, "task");

            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string query = "UPDATE [dbo].[ToDoListTasks] SET [TaskStatus] = @TaskStatus, [DueDate] = @DueDate,[Important] = @Important,[Urgent] = @Urgent, [ToDoListId] = @ToDoListId, [Title] = @Title, [Description] = @Description,[DateCreation] = @DateCreation,[Remind] = @Remind WHERE [Id]=@Id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.Add("@TaskStatus", SqlDbType.TinyInt).Value = (byte)task.TaskStatus;
                cmd.Parameters.Add("@DueDate", SqlDbType.DateTime2).Value = task.DueDate;
                cmd.Parameters.Add("@Important", SqlDbType.Bit).Value = task.Important;
                cmd.Parameters.Add("@Urgent", SqlDbType.Bit).Value = task.Urgent;
                cmd.Parameters.Add("@ToDoListId", SqlDbType.BigInt).Value = task.ToDoListId;
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = task.Title;
                cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = task.Description;
                cmd.Parameters.Add("@DateCreation", SqlDbType.DateTime2).Value = task.DateCreation;
                cmd.Parameters.Add("@Remind", SqlDbType.DateTime2).Value = (object?)task.Remind ?? DBNull.Value;
                cmd.Parameters.Add("@Id", SqlDbType.BigInt).Value = task.Id;
                con.Open();
                await cmd.ExecuteNonQueryAsync();
            }
        }

        /// <inheritdoc />
        public async Task DeleteAsync(ToDoListTask task)
        {
            Helpers.IfNullException(task, "task");

            using (SqlConnection con = new(this.connectionString))
            {
                string query = "Delete from [ToDoListTasks] where [Id] = @Id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.Add("@Id", SqlDbType.BigInt).Value = (long)task.Id;
                con.Open();
                await cmd.ExecuteNonQueryAsync();
            }
        }
    }
}
