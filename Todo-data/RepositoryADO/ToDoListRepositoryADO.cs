﻿using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Todo_data.Interfaces;
using Todo_domain_entities;

namespace Todo_data.RepositoryADO
{
    /// <summary>
    /// To-Do list ADO Sql repository.
    /// </summary>
    public class ToDoListRepositoryADO : IToDoListRepository
    {
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListRepositoryADO"/> class.
        /// </summary>
        /// <param name="connectionString">connection String.</param>
        public ToDoListRepositoryADO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <inheritdoc />
        public async Task<IQueryable<ToDoList>> GetAllAsync()
        {
            List<ToDoList> lists = new List<ToDoList>();
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string query = "SELECT ToDoLists.* FROM ToDoLists";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader rdr = await cmd.ExecuteReaderAsync();
                while (await rdr.ReadAsync())
                {
                    List<ToDoListTask> tasks = new List<ToDoListTask>();
                    string queryTask = "SELECT ToDoListTasks.* FROM ToDoListTasks WHERE  (ToDoListId = @listID)";
                    SqlCommand cmdTask = new SqlCommand(queryTask, con);
                    cmdTask.Parameters.Add("@listID", SqlDbType.BigInt).Value = (long)rdr["Id"];
                    SqlDataReader rdrTask = await cmdTask.ExecuteReaderAsync();
                    while (await rdrTask.ReadAsync())
                    {
                        tasks.Add(new ToDoListTask()
                        {
                            Id = (long)rdrTask["Id"],
                            DateCreation = (DateTime)rdrTask["DateCreation"],
                            Description = (string)rdrTask["Description"],
                            Title = (string)rdrTask["Title"],
                            DueDate = (DateTime)rdrTask["DueDate"],
                            Important = (bool)rdrTask["Important"],
                            Urgent = (bool)rdrTask["Urgent"],
                            Remind = (rdrTask["Remind"] is not DBNull) ? (DateTime)rdrTask["Remind"] : null,
                            ToDoListId = (long)rdrTask["ToDoListId"],
                            TaskStatus = (Status)rdrTask["TaskStatus"],
                        });
                    }

                    lists.Add(new ToDoList()
                    {
                        Id = (long)rdr["Id"],
                        Title = (string)rdr["Title"],
                        Description = (string)rdr["Description"],
                        Hide = (bool)rdr["Hide"],
                        DateCreation = (DateTime)rdr["DateCreation"],
                        UserId = (string)rdr["UserId"],
                        ToDoListTasks = tasks,
                    });

                    rdrTask.Close();
                }

                return lists.AsQueryable();
            }
        }

        /// <inheritdoc />
        public async Task<long> AddAsync(ToDoList list)
        {
            Helpers.IfNullException(list, "list");

            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string query = "INSERT INTO [dbo].[ToDoLists] ([Hide], [Title], [Description], [DateCreation], [UserId]) VALUES (@Hide, @Title, @Description, @DateCreation, @UserId)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.Add("@Hide", SqlDbType.Bit).Value = list.Hide;
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = list.Title;
                cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = list.Description;
                cmd.Parameters.Add("@DateCreation", SqlDbType.DateTime2).Value = list.DateCreation;
                cmd.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = list.UserId;

                con.Open();
                await cmd.ExecuteNonQueryAsync();
            }

            var listAdded = (await this.GetAllAsync()).FirstOrDefault(x => x.DateCreation >= list.DateCreation && x.UserId == list.UserId && x.Title == list.Title);
            return listAdded!.Id;
        }

        /// <inheritdoc />
        public async Task DeleteAsync(ToDoList list)
        {
            Helpers.IfNullException(list, "list");

            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string query = "DELETE FROM ToDoLists WHERE  (Id = @Id)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = list.Id;

                con.Open();
                await cmd.ExecuteNonQueryAsync();
            }
        }

        /// <inheritdoc />
        public async Task UpdateAsync(ToDoList list)
        {
            Helpers.IfNullException(list, "list");

            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string query = "UPDATE [dbo].[ToDoLists] SET [Hide] = @Hide, [Title] = @Title, [Description] = @Description WHERE  (Id = @Id)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = list.Id;
                cmd.Parameters.Add("@Hide", SqlDbType.Bit).Value = list.Hide;
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = list.Title;
                cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = list.Description;

                con.Open();
                await cmd.ExecuteNonQueryAsync();
            }
        }
    }
}
