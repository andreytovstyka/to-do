﻿namespace Todo_data
{
    using Microsoft.EntityFrameworkCore;
    using Todo_domain_entities;

    /// <summary>
    /// To-Do Context.
    /// </summary>
    public class ToDoContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoContext"/> class.
        /// </summary>
        /// <param name="options">options.</param>
        public ToDoContext(DbContextOptions<ToDoContext> options)
          : base(options)
        {
        }

        /// <summary>
        /// Gets dbSet ToDoList.
        /// </summary>
        public DbSet<ToDoList> ToDoLists => this.Set<ToDoList>();

        /// <summary>
        /// Gets dbSet ToDoListTask.
        /// </summary>
        public DbSet<ToDoListTask> ToDoListTasks => this.Set<ToDoListTask>();
    }
}
