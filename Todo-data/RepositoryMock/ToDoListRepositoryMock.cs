﻿namespace Todo_data.RepositoryMock
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using Todo_data.Interfaces;
    using Todo_domain_entities;

    /// <summary>
    /// Mock To-Do List.
    /// </summary>
    public class ToDoListRepositoryMock : IToDoListRepository
    {
        /// <summary>
        /// Gets or sets ToDoList.
        /// </summary>
        public List<ToDoList> Lists { get; set; } = new List<ToDoList>();

        /// <inheritdoc />
        public async Task<IQueryable<ToDoList>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return this.Lists.AsQueryable().Include(t => t.ToDoListTasks);
            });
        }

        /// <inheritdoc />
        public async Task<long> AddAsync(ToDoList list)
        {
            return await Task.Run(() =>
            {
                Helpers.IfNullException(list, "list");
                list.Id = this.Lists.Count + 1;
                this.Lists.Add(list);
                return list.Id;
            });
        }

        /// <inheritdoc />
        public async Task DeleteAsync(ToDoList list)
        {
            await Task.Run(() =>
            {
                Helpers.IfNullException(list, "list");
                this.Lists.Remove(list);
            });
        }

        /// <inheritdoc />
        public async Task UpdateAsync(ToDoList list)
        {
            await Task.Run(() =>
            {
                Helpers.IfNullException(list, "list");

                ToDoList? updtList = this.Lists.FirstOrDefault(x => x.Id == list.Id);
                if (updtList != null)
                {
                    updtList.Title = list.Title;
                    updtList.Description = list.Description;
                }
            });
        }
    }
}
