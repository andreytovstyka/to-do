﻿namespace Todo_data.RepositoryMock
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Todo_data.Interfaces;
    using Todo_domain_entities;

    /// <summary>
    /// Mock To-Do List Task.
    /// </summary>
    public class ToDoListTaskRepositoryMock : IToDoListTaskRepository
    {
        /// <summary>
        /// Gets or sets ToDoListTask.
        /// </summary>
        public List<ToDoListTask> Tasks { get; set; } = new List<ToDoListTask>();

        /// <inheritdoc />
        public async Task<IQueryable<ToDoListTask>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return this.Tasks.AsQueryable();
            });
        }

        /// <inheritdoc />
        public async Task AddAsync(ToDoListTask task)
        {
            await Task.Run(() =>
            {
                Helpers.IfNullException(task, "task");
                task.Id = this.Tasks.Count + 1;
                this.Tasks.Add(task);
            });
        }

        /// <inheritdoc />
        public async Task UpdateAsync(ToDoListTask task)
        {
            await Task.Run(() =>
            {
                Helpers.IfNullException(task, "task");

                ToDoListTask? updtTask = this.Tasks.FirstOrDefault(x => x.Id == task.Id);
                if (updtTask != null)
                {
                    if (updtTask.Title != task.Title)
                    {
                        updtTask.Title = task.Title;
                    }

                    if (updtTask.Description != task.Description)
                    {
                        updtTask.Description = task.Description;
                    }

                    if (updtTask.DueDate != task.DueDate)
                    {
                        updtTask.DueDate = task.DueDate;
                    }

                    if (updtTask.Important != task.Important)
                    {
                        updtTask.Important = task.Important;
                    }

                    if (updtTask.Urgent != task.Urgent)
                    {
                        updtTask.Urgent = task.Urgent;
                    }

                    if (updtTask.Remind != task.Remind)
                    {
                        updtTask.Remind = task.Remind;
                    }

                    if (updtTask.TaskStatus != task.TaskStatus)
                    {
                        updtTask.TaskStatus = task.TaskStatus;
                    }
                }
            });
        }

        /// <inheritdoc />
        public async Task DeleteAsync(ToDoListTask task)
        {
            await Task.Run(() =>
            {
                Helpers.IfNullException(task, "task");
                this.Tasks.Remove(task);
            });
        }
    }
}
