﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Todo_data.Migrations
{
#pragma warning disable SA1601 // Partial elements should be documented
    /// <summary>
    /// Migration.
    /// </summary>
    [ExcludeFromCodeCoverageAttribute]
    public partial class Control : Migration
#pragma warning restore SA1601 // Partial elements should be documented
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // This code created VS.
        }

        /// <summary>
        /// This code created VS.
        /// </summary>
        /// <param name="migrationBuilder">builder.</param>
        protected override void Down(MigrationBuilder migrationBuilder) // This code created VS.
        {
            // This code created VS.
        }
    }
}
