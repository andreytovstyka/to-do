﻿namespace Todo_data.RepositorySql
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using Todo_data.Interfaces;
    using Todo_domain_entities;

    /// <summary>
    /// To-Do list Sql repository.
    /// </summary>
    public class ToDoListRepositorySql : IToDoListRepository
    {
        private readonly ToDoContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListRepositorySql"/> class.
        /// </summary>
        /// <param name="toDoContext">context.</param>
        public ToDoListRepositorySql(ToDoContext toDoContext)
        {
            this.db = toDoContext;
        }

        /// <inheritdoc />
        public async Task<IQueryable<ToDoList>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return this.db.ToDoLists.Include(t => t.ToDoListTasks);
            });
        }

        /// <inheritdoc />
        public async Task<long> AddAsync(ToDoList list)
        {
            Helpers.IfNullException(list, "list");

            var newList = this.db.ToDoLists.Add(list);
            await this.db.SaveChangesAsync();

            return newList.Entity.Id;
        }

        /// <inheritdoc />
        public async Task DeleteAsync(ToDoList list)
        {
            Helpers.IfNullException(list, "list");

            this.db.ToDoLists.Remove(list);
            await this.db.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task UpdateAsync(ToDoList list)
        {
            Helpers.IfNullException(list, "list");

            ToDoList? toDoList = this.db.ToDoLists.FirstOrDefault(x => x.Id == list.Id);

            if (toDoList != null)
            {
                toDoList.Title = list.Title;
                toDoList.Description = list.Description;

                await this.db.SaveChangesAsync(); 
            }
        }
    }
}
