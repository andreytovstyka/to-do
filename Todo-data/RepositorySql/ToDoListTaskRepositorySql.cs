﻿namespace Todo_data.RepositorySql
{
    using System.Linq;
    using Todo_data.Interfaces;
    using Todo_domain_entities;

    /// <summary>
    /// To-Do list Sql repository.
    /// </summary>
    public class ToDoListTaskRepositorySql : IToDoListTaskRepository
    {
        private readonly ToDoContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListTaskRepositorySql"/> class.
        /// </summary>
        /// <param name="toDoContext">context.</param>
        public ToDoListTaskRepositorySql(ToDoContext toDoContext)
        {
            this.db = toDoContext;
        }

        /// <inheritdoc />
        public async Task<IQueryable<ToDoListTask>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return this.db.ToDoListTasks;
            });
        }

        /// <inheritdoc />
        public async Task AddAsync(ToDoListTask task)
        {
            Helpers.IfNullException(task, "task");

            this.db.ToDoListTasks.Add(task);

            await this.db.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task UpdateAsync(ToDoListTask task)
        {
            Helpers.IfNullException(task, "task");

            var updtTask = this.db.ToDoListTasks.FirstOrDefault(x => x.Id == task.Id);

            if (updtTask != null)
            {
                if (updtTask.Title != task.Title)
                {
                    updtTask.Title = task.Title;
                }

                if (updtTask.Description != task.Description)
                {
                    updtTask.Description = task.Description;
                }

                if (updtTask.DueDate != task.DueDate)
                {
                    updtTask.DueDate = task.DueDate;
                }

                if (updtTask.Remind != task.Remind)
                {
                    updtTask.Remind = task.Remind;
                }

                if (updtTask.Important != task.Important)
                {
                    updtTask.Important = task.Important;
                }

                if (updtTask.Urgent != task.Urgent)
                {
                    updtTask.Urgent = task.Urgent;
                }

                if (updtTask.TaskStatus != task.TaskStatus)
                {
                    updtTask.TaskStatus = task.TaskStatus;
                }

                await this.db.SaveChangesAsync();
            }
        }

        /// <inheritdoc />
        public async Task DeleteAsync(ToDoListTask task)
        {
            Helpers.IfNullException(task, "task");

            this.db.ToDoListTasks.Remove(task);
            await this.db.SaveChangesAsync();
        }
    }
}
