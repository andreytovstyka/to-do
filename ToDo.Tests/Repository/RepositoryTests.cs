﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Todo_data;
using Todo_data.Interfaces;
using Todo_data.RepositoryADO;
using Todo_data.RepositorySql;
using Todo_domain_entities;
using Todo_services;

namespace Repository.Tests
{
    public class RepositoryTests
    {
        private static readonly string TestDbConnectionString = "Server=(LocalDB)\\MSSQLLocalDB;Database=ToDoTestDB;Trusted_Connection=True;MultipleActiveResultSets=true;";
        private static ToDoListRepositorySql listRepositoryEfSql;
        private static ToDoListTaskRepositorySql taskRepositoryEfSql;
        private static ToDoContext toDoContext;

        /// <summary>
        /// Apply migrations.
        /// </summary>
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            if (toDoContext.Database.GetPendingMigrations().Any())
            {
                toDoContext.Database.Migrate();
            }
        }

        /// <summary>
        /// Run Befor Any Tests.
        /// </summary>
        /// <returns>representing the asynchronous method.</returns>
        [SetUp]
        public async Task RunBeforAnyTests()
        {
            toDoContext.Database.ExecuteSqlRaw("DELETE FROM [dbo].[ToDoLists];");
            toDoContext.Database.ExecuteSqlRaw("DELETE FROM [dbo].[ToDoListTasks];");
            await SeedDate.SeedAsync(listRepositoryEfSql, taskRepositoryEfSql, userId: "test");
        }

        /// <summary>
        /// Run after all tests.
        /// </summary>
        /// <returns>representing the asynchronous method.</returns>
        [OneTimeTearDown]
        public async Task DeleteTestDB()
        {
            await toDoContext.Database.EnsureDeletedAsync();
        }

        /// <summary>
        /// Check if all lists are returned.
        /// </summary>
        /// <param name="listRepo">list repository.</param>
        /// <returns>representing the asynchronous method.</returns>
        [TestCaseSource(nameof(ListRepos))]
        public async Task ListGetAll_Seed_ReturnedList(IToDoListRepository listRepo)
        {
            List<ToDoList> expectedLists = new List<ToDoList>
            {
                new ToDoList
                {
                    Id = 1,
                    Title = "Home",
                    Description = "home & family",
                    UserId = "test",
                    Hide = false,
                },
                new ToDoList
                {
                    Id = 2,
                    Title = "Job",
                    Description = "job & career",
                    UserId = "test",
                    Hide = false,
                },
                new ToDoList
                {
                    Id = 3,
                    Title = "Hobby",
                    Description = "favorite activities & health",
                    UserId = "test",
                    Hide = false,
                },
            };

            var act = (await listRepo.GetAllAsync()).ToList();

            if (act.Count != expectedLists.Count)
            {
                Assert.Fail();
            }

            for (int i = 0; i < expectedLists.Count; i++)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(expectedLists[i].UserId, Is.EqualTo(act[i].UserId));
                    Assert.That(expectedLists[i].Hide, Is.EqualTo(act[i].Hide));
                    Assert.That(expectedLists[i].Title, Is.EqualTo(act[i].Title));
                    Assert.That(expectedLists[i].Description, Is.EqualTo(act[i].Description));
                });
            }

            foreach (var list in act)
            {
                await listRepo.DeleteAsync(list);
            }

            act = (await listRepo.GetAllAsync()).ToList();
            Assert.That(act.Count(), Is.EqualTo(0));
        }

        /// <summary>
        /// Checking if the list has been added to the database.
        /// </summary>
        /// <param name="listRepo">list repository.</param>
        /// <returns>representing the asynchronous unit test.</returns>
        [TestCaseSource(nameof(ListRepos))]
        public async Task ListAdd_NewList_ListAdded(IToDoListRepository listRepo)
        {
            ToDoList newList = new()
            {
                Description = "Test Description",
                UserId = "test",
                Title = "Test Title",
                Hide = true,
            };

            var newId = await listRepo.AddAsync(newList);

            var testList = (await listRepo.GetAllAsync()).FirstOrDefault(x => x.Id == newId);
            Assert.Multiple(() =>
            {
                Assert.That(testList.UserId, Is.EqualTo(newList.UserId));
                Assert.That(testList.Hide, Is.EqualTo(newList.Hide));
                Assert.That(testList.Title, Is.EqualTo(newList.Title));
                Assert.That(testList.Description, Is.EqualTo(newList.Description));
            });
        }

        /// <summary>
        /// Checking if the list has been deleted from the database.
        /// </summary>
        /// <param name="listRepo">list repository.</param>
        /// <returns>representing the asynchronous unit test.</returns>
        [TestCaseSource(nameof(ListRepos))]
        public async Task ListDelete_List_ListDeleted(IToDoListRepository listRepo)
        {
            ToDoList deletingList = (await listRepo.GetAllAsync()).FirstOrDefault();
            var deletingListId = deletingList.Id;
            await listRepo.DeleteAsync(deletingList);

            Assert.That((await listRepo.GetAllAsync()).Any(x => x.Id == deletingListId), Is.False);
        }

        /// <summary>
        /// Checking if the list has been updated.
        /// </summary>
        /// <param name="listRepo">list repository.</param>
        /// <returns>representing the asynchronous unit test.</returns>
        [TestCaseSource(nameof(ListRepos))]
        public async Task ListUpdate_NewList_ListUpdated(IToDoListRepository listRepo)
        {
            ToDoList updatingLists = (await listRepo.GetAllAsync()).FirstOrDefault();

            var updatingListsId = updatingLists.Id;

            updatingLists.Description = "Test Description";
            updatingLists.UserId = "test";
            updatingLists.Title = "Test Title";
            updatingLists.Hide = false;

            await listRepo.UpdateAsync(updatingLists);

            var testList = (await listRepo.GetAllAsync()).FirstOrDefault(x => x.Id == updatingListsId);
            Assert.Multiple(() =>
            {
                Assert.That(testList.UserId, Is.EqualTo("test"));
                Assert.That(testList.Hide, Is.EqualTo(false));
                Assert.That(testList.Title, Is.EqualTo("Test Title"));
                Assert.That(testList.Description, Is.EqualTo("Test Description"));
            });
        }

        /// <summary>
        /// Check if all tasks are returned.
        /// </summary>
        /// <param name="taskRepo">task repository.</param>
        /// <returns>representing the asynchronous method.</returns>
        [TestCaseSource(nameof(TaskRepos))]
        public async Task TaskGetAll_Seed_ReturnedTasks(IToDoListTaskRepository taskRepo)
        {
            List<ToDoListTask> expectedTasks = new List<ToDoListTask>
            {
                new ToDoListTask
                {
                    Title = "Budget planning",
                    Description = "next month",
                    DueDate = DateTime.Now.Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Title = "Buy a climbing frame for a cat",
                    Description = "with a place to sleep",
                    DueDate = DateTime.Now.AddDays(2).Date,
                    Important = true,
                    Urgent = false,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Title = "Do To-Do List application.",
                    Description = "EPAM training",
                    DueDate = DateTime.Now.AddDays(5).Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Title = "Do First EF Core application.",
                    Description = "EPAM training",
                    DueDate = DateTime.Now.Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Completed,
                },
                new ToDoListTask
                {
                    Title = "Do ADO.NET application.",
                    Description = "EPAM training",
                    DueDate = DateTime.Now.AddDays(5).Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Title = "Run a marathon.",
                    Description = "everything is possible",
                    DueDate = DateTime.Now.AddYears(1).Date,
                    Important = true,
                    Urgent = false,
                    TaskStatus = Status.Completed,
                },
                new ToDoListTask
                {
                    Title = "See the Grand Canyon.",
                    Description = "at first think of a plan",
                    DueDate = DateTime.Now.AddYears(2).Date,
                    Important = true,
                    Urgent = false,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Title = "Buy a fishing rod.",
                    Description = "2 is better",
                    DueDate = DateTime.Now.AddDays(20).Date,
                    Important = false,
                    Urgent = false,
                    TaskStatus = Status.Created,
                },
            };

            var act = (await taskRepo.GetAllAsync()).ToList();

            if (act.Count != expectedTasks.Count)
            {
                Assert.Fail();
            }

            for (int i = 0; i < expectedTasks.Count; i++)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(expectedTasks[i].Important, Is.EqualTo(act[i].Important));
                    Assert.That(expectedTasks[i].Description, Is.EqualTo(act[i].Description));
                    Assert.That(expectedTasks[i].DueDate, Is.EqualTo(act[i].DueDate));
                    Assert.That(expectedTasks[i].TaskStatus, Is.EqualTo(act[i].TaskStatus));
                    Assert.That(expectedTasks[i].Title, Is.EqualTo(act[i].Title));
                    Assert.That(expectedTasks[i].Urgent, Is.EqualTo(act[i].Urgent));
                });
            }
        }

        /// <summary>
        /// The test of adding a new task to db.
        /// </summary>
        /// <param name="taskRepo">task repository.</param>
        /// <returns>representing the asynchronous unit test.</returns>
        [TestCaseSource(nameof(TaskRepos))]
        public async Task TaskAdd_CreatedTask_NewTaskAdded(IToDoListTaskRepository taskRepo)
        {
            var listId = (await listRepositoryEfSql.GetAllAsync()).FirstOrDefault().Id;

            ToDoListTask toDoTask = new ToDoListTask
            {
                Title = "New task",
                Description = "The test of adding a new task.",
                DueDate = DateTime.Now.AddDays(1),
                TaskStatus = Status.Created,
                ToDoListId = listId,
            };

            await taskRepo.AddAsync(toDoTask);

            var act = (await taskRepo.GetAllAsync()).Any(x => x.Title == "New task");

            Assert.That(act, Is.True);
        }

        /// <summary>
        /// The test of updating a task.
        /// </summary>
        /// <param name="taskRepo">task repository.</param>
        /// <returns>representing the asynchronous unit test.</returns>
        [TestCaseSource(nameof(TaskRepos))]
        public async Task TaskUpdate_UpdatedTask_TaskUpdated(IToDoListTaskRepository taskRepo)
        {
            foreach (var initTask in (await taskRepo.GetAllAsync()).ToList())
            {
                ToDoListTask updatingTask = new ToDoListTask()
                {
                    Description = "New description",
                    DueDate = initTask.DueDate.AddDays(1),
                    Id = initTask.Id,
                    Important = !initTask.Important,
                    Remind = null,
                    TaskStatus = initTask.TaskStatus == Status.Completed ? Status.Started : (Status)((byte)initTask.TaskStatus + 1),
                    Title = "New Title",
                    ToDoListId = initTask.ToDoListId,
                    Urgent = !initTask.Urgent,
                };

                await taskRepo.UpdateAsync(updatingTask);

                var updatedTask = (await taskRepo.GetAllAsync()).FirstOrDefault(x => x.Id == initTask.Id);

                Assert.That(updatedTask.Id, Is.EqualTo(updatingTask.Id));
                Assert.That(updatedTask.Important, Is.EqualTo(updatingTask.Important));
                Assert.That(updatedTask.Description, Is.EqualTo(updatingTask.Description));
                Assert.That(updatedTask.DueDate, Is.EqualTo(updatingTask.DueDate));
                Assert.That(updatedTask.Remind, Is.EqualTo(updatingTask.Remind));
                Assert.That(updatedTask.TaskStatus, Is.EqualTo(updatingTask.TaskStatus));
                Assert.That(updatedTask.Title, Is.EqualTo(updatingTask.Title));
                Assert.That(updatedTask.ToDoListId, Is.EqualTo(updatingTask.ToDoListId));
                Assert.That(updatedTask.Urgent, Is.EqualTo(updatingTask.Urgent));
            }
        }

        /// <summary>
        /// Checking if the task has been deleted from the db.
        /// </summary>
        /// <param name="taskRepo">task repository.</param>
        /// <returns>representing the asynchronous unit test.</returns>
        [TestCaseSource(nameof(TaskRepos))]
        public async Task TaskDelete_Task_TaskDeleted(IToDoListTaskRepository taskRepo)
        {
            ToDoListTask deletingTask = (await taskRepo.GetAllAsync()).FirstOrDefault();
            var deletingTaskId = deletingTask.Id;
            await taskRepo.DeleteAsync(deletingTask);

            Assert.That((await taskRepo.GetAllAsync()).Any(x => x.Id == deletingTaskId), Is.False);
        }

        /// <summary>
        /// Initialization of ToDoIdentityDbContext test database.
        /// </summary>
        [Test]
        public void ToDoIdentityDbContext_Set_Pass()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ToDoIdentityDbContext>();
            optionsBuilder.UseSqlServer(TestDbConnectionString);

            ToDoIdentityDbContext toDoIdentityDb = new ToDoIdentityDbContext(optionsBuilder.Options);
            Assert.Pass();
        }

        private static IEnumerable<IToDoListRepository> ListRepos()
        {
            SetToDoContext();

            listRepositoryEfSql = new ToDoListRepositorySql(toDoContext);

            yield return listRepositoryEfSql;

            yield return new ToDoListRepositoryADO(TestDbConnectionString);
        }

        private static IEnumerable<IToDoListTaskRepository> TaskRepos()
        {
            SetToDoContext();

            taskRepositoryEfSql = new ToDoListTaskRepositorySql(toDoContext);

            yield return taskRepositoryEfSql;

            yield return new ToDoListTaskRepositoryADO(TestDbConnectionString);
        }

        /// <summary>
        /// Initialization of ToDoContext test database.
        /// </summary>
        private static void SetToDoContext()
        {
            if (toDoContext is null)
            {
                var optionsBuilder = new DbContextOptionsBuilder<ToDoContext>();
                optionsBuilder.UseSqlServer(TestDbConnectionString);

                toDoContext = new ToDoContext(optionsBuilder.Options);
            }
        }
    }
}
