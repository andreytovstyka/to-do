using System.ComponentModel.DataAnnotations;

namespace TodoDomainEntities.Tests
{
    /// <summary>
    /// A class for testing minimal and maximum length of the line from the attribute.
    /// </summary>
    public class TestClass
    {
        /// <summary>
        /// Gets or sets. Property with StringLength attribute for testing.
        /// </summary>
        [StringLength(100, MinimumLength = 10)]
        public string TestPropertyWithStringLength { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets. Property without StringLength attribute for testing.
        /// </summary>
        public string TestPropertyWithoutStringLength { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets. Property with invalid attribute values (min>max).
        /// </summary>
        [StringLength(10, MinimumLength = 100)]
        public string TestPropertyWithInvalidAttributeValues { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets. Property with max value less than 0.
        /// </summary>
        [StringLength(-2, MinimumLength = -10)]
        public string TestPropertyWithMaxValuesLessZero { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets. Property with min value less than 0.
        /// </summary>
        [StringLength(2, MinimumLength = -10)]
        public string TestPropertyWithMinValuesLessZero { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets. Property with null min value.
        /// </summary>
        [StringLength(2)]
        public string TestPropertyWithNullMinValue { get; set; } = string.Empty;
    }
}
