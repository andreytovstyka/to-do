using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Todo_domain_entities;

namespace TodoDomainEntities.Tests
{
    /// <summary>
    /// Helpers Tests.
    /// </summary>
    public class EntitiesHelpersTests
    {
        /// <summary>
        /// Checking if an invalid string matches the parameters.
        /// </summary>
        /// <param name = "checkStr" > string.</ param >
        /// <param name = "minLenght" > min Lenght.</ param >
        /// <param name = "maxLenght" > max Lenght.</ param >
        [TestCase("Hello, word!", 5, 11)]
        [TestCase("Hello, word!", 13, 100)]
        public void IfStringLengthOutOfRangeException_InvalidString_ArgumentOutOfRangeException(string checkStr, int minLenght, int maxLenght)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Helpers.IfStringLengthOutOfRangeException(checkStr, "invalidString", minLenght, maxLenght));
        }

        /// <summary>
        /// Checking if an valid string matches the parameters.
        /// </summary>
        /// <param name = "checkStr" > string.</ param >
        /// <param name = "minLenght" > min Lenght.</ param >
        /// <param name = "maxLenght" > max Lenght.</ param >
        [TestCase("Hello, word!", 5, 12)]
        [TestCase("Hello, word!", 12, 100)]
        public void IfStringLengthOutOfRangeException_ValidString_Pass(string checkStr, int minLenght, int maxLenght)
        {
            Helpers.IfStringLengthOutOfRangeException(checkStr, "validString", minLenght, maxLenght);
            Assert.Pass();
        }

        /// <summary>
        /// Attempt to pass min value grater than max.
        /// </summary>
        /// <param name = "checkStr" > string.</ param >
        /// <param name = "minLenght" > min Lenght.</ param >
        /// <param name = "maxLenght" > max Lenght.</ param >
        [TestCase("Hello, word!", 2, 1)]
        public void IfStringLengthOutOfRangeException_MinGraterMax_ArgumentException(string checkStr, int minLenght, int maxLenght)
        {
            Assert.Throws<ArgumentException>(() => Helpers.IfStringLengthOutOfRangeException(checkStr, "String", minLenght, maxLenght));
        }

        /// <summary>
        /// Attempt to pass first value grater than second.
        /// </summary>
        [Test]
        public void IfFirstGraterSecondException_FirstGraterSecondException_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => Helpers.IfFirstGraterThanSecondException(2, 1));
        }

        /// <summary>
        /// Check if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void IfNullOrEmptyException_null_ArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => Helpers.IfNullOrEmptyException(null, "paramName"));
        }

        /// <summary>
        /// Check if an exception will be thrown if empty is passed.
        /// </summary>
        [Test]
        public void IfNullOrEmptyException_empty_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => Helpers.IfNullOrEmptyException(string.Empty, "paramName"));
        }

        /// <summary>
        /// Check if an exception will be thrown if valid value is passed.
        /// </summary>
        [Test]
        public void IfNullOrEmptyException_valid_Pass()
        {
            Helpers.IfNullOrEmptyException("Hello!", "paramName");

            Assert.Pass();
        }

        /// <summary>
        /// Pass past date.
        /// </summary>
        [Test]
        public void IsDateTodayOrFuture_PastDate_False()
        {
            var act = Helpers.IsDateTodayOrFuture(DateTime.Now.AddDays(-1));

            Assert.That(act, Is.False);
        }

        /// <summary>
        /// Pass today date.
        /// </summary>
        [Test]
        public void IsDateTodayOrFuture_Today_True()
        {
            var act = Helpers.IsDateTodayOrFuture(DateTime.Now);

            Assert.That(act, Is.True);
        }

        /// <summary>
        /// Pass future date.
        /// </summary>
        [Test]
        public void IsDateTodayOrFuture_Future_True()
        {
            var act = Helpers.IsDateTodayOrFuture(DateTime.Now.AddDays(1));

            Assert.That(act, Is.True);
        }

        /// <summary>
        /// Check if an exception will be thrown if past date is passed.
        /// </summary>
        [Test]
        public void IfDatePastException_Past_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => Helpers.IfDatePastException(DateTime.Now.AddDays(-1), "ParName"));
        }

        /// <summary>
        /// Check if an exception will be thrown if today date is passed.
        /// </summary>
        [Test]
        public void IfDatePastException_Today_Pass()
        {
            Helpers.IfDatePastException(DateTime.Now, "ParName");

            Assert.Pass();
        }

        /// <summary>
        /// Check if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void IfNullException_Null_ArgumentNullException()
        {
            string val = null;

            Assert.Throws<ArgumentNullException>(() => Helpers.IfNullException(val, "ParName"));
        }

        /// <summary>
        /// Check if an exception will be thrown if not null is passed.
        /// </summary>
        [Test]
        public void IfNullException_Valid_Pass()
        {
            Helpers.IfNullException("Hello", "ParName");

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to pass missing property.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_MissingProperty_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>("MissingProperty"));
        }

        /// <summary>
        /// Attempt to pass null property.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_NullProperty_ArgumentException()
        {
            (int min, int max) = Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>(null);

            Assert.Multiple(() =>
            {
                Assert.That(min, Is.EqualTo(0));
                Assert.That(max, Is.EqualTo(int.MaxValue));
            });
        }

        /// <summary>
        /// Attempt to pass min null value.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_NullMinProperty_ArgumentException()
        {
            (int min, int max) = Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>("TestPropertyWithNullMinValue");

            Assert.Multiple(() =>
            {
                Assert.That(min, Is.EqualTo(0));
                Assert.That(max, Is.EqualTo(2));
            });
        }

        /// <summary>
        /// Attempt to pass max value less than zero.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_MaxLessZero_ArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>("TestPropertyWithMaxValuesLessZero"));
        }

        /// <summary>
        /// Attempt to pass min value less than zero.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_MinLessZero_ArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>("TestPropertyWithMinValuesLessZero"));
        }

        /// <summary>
        /// Attempt to pass property with min 10 and max 100 values.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_PropertyWithStringLengthAttribute_10_100()
        {
            int minExpected = 10;
            int maxExpected = 100;

            (int min, int max) = Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>("TestPropertyWithStringLength");

            Assert.Multiple(() =>
            {
                Assert.That(min, Is.EqualTo(minExpected));
                Assert.That(max, Is.EqualTo(maxExpected));
            });
        }

        /// <summary>
        /// Attempt to pass property without StringLength attribute.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_PropertyWithoutStringLengthAttribute_0_maxInt()
        {
            int minExpected = 0;
            int maxExpected = int.MaxValue;

            (int actMin, int actMax) = Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>("TestPropertyWithoutStringLength");

            Assert.Multiple(() =>
            {
                Assert.That(actMin, Is.EqualTo(minExpected));
                Assert.That(actMax, Is.EqualTo(maxExpected));
            });
        }

        /// <summary>
        /// Attempt to pass property with invalid attribute values.
        /// </summary>
        [Test]
        public void GetMinAndMaxLenghtOfProperty_PropertyWithInvalidAttributeValues_ArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentException>(() => Helpers.GetMinAndMaxValueOfStringLengthAttribute<TestClass>("TestPropertyWithInvalidAttributeValues"));
        }

        /// <summary>
        /// Checking if an exception will be thrown when the value does not match a custom attribute.
        /// </summary>
        [Test]
        public void IfInvalidCustomAttributesException_InvalidString_ArgumentException()
        {
            Assert.Throws<ArgumentException>(()
                => Helpers.IfInvalidCustomAttributesException<TestClass, string>("TestPropertyWithStringLength", "test"));
        }

        /// <summary>
        /// Checking if the validation will pass when the value matchs the custom attribute.
        /// </summary>
        [Test]
        public void IfInvalidCustomAttributesException_ValidString_TestPass()
        {
            Helpers.IfInvalidCustomAttributesException<TestClass, string>("TestPropertyWithStringLength", "test valid");
            Assert.Pass();
        }
    }
}
