﻿using NUnit.Framework;
using Todo_domain_entities;

namespace TodoDomainEntities.Tests
{
    /// <summary>
    /// Entities Tests.
    /// </summary>
    public class EntitiesTests
    {
        private static int minTaskTitleLengh;
        private static int maxTaskTitleLengh;
        private static int minTaskDescriptionLengh;
        private static int maxTaskDescriptionLengh;
        private static int minListTitleLengh;
        private static int maxListTitleLengh;
        private static int minListDescriptionLengh;
        private static int maxListDescriptionLengh;

        /// <summary>
        /// Calculate min and max lenght of propereties.
        /// </summary>
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            var limits = Helpers.GetMinAndMaxValueOfStringLengthAttribute<ToDoList>("Title");
            minListTitleLengh = limits.minLenght;
            maxListTitleLengh = limits.maxLenght;
            limits = Helpers.GetMinAndMaxValueOfStringLengthAttribute<ToDoList>("Description");
            minListDescriptionLengh = limits.minLenght;
            maxListDescriptionLengh = limits.maxLenght;
            limits = Helpers.GetMinAndMaxValueOfStringLengthAttribute<ToDoListTask>("Title");
            minTaskTitleLengh = limits.minLenght;
            maxTaskTitleLengh = limits.maxLenght;
            limits = Helpers.GetMinAndMaxValueOfStringLengthAttribute<ToDoListTask>("Description");
            minTaskDescriptionLengh = limits.minLenght;
            maxTaskDescriptionLengh = limits.maxLenght;
        }

        /// <summary>
        /// Attempt to create list with null title.
        /// </summary>
        [Test]
        public void ToDoList_NullTitle_ArgumentNullException()
        {
            ToDoList list = new ToDoList();

            Assert.Throws<ArgumentNullException>(() => list.Title = null);
        }

        /// <summary>
        /// Attempt to create list with empty title.
        /// </summary>
        [Test]
        public void ToDoList_EmptyTitle_ArgumentException()
        {
            ToDoList list = new ToDoList();

            Assert.Throws<ArgumentException>(() => list.Title = string.Empty);
        }

        /// <summary>
        /// Attempt to create a title that is too short.
        /// </summary>
        [Test]
        public void ToDoList_ShortTitle_ArgumentException()
        {
            ToDoList list = new ToDoList();

            if (minTaskTitleLengh > 0)
            {
                Assert.Throws<ArgumentException>(() => list.Title = new string('a', minListTitleLengh - 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create a title that is too long.
        /// </summary>
        [Test]
        public void ToDoList_LongTitle_ArgumentException()
        {
            ToDoList list = new ToDoList();

            if (maxTaskTitleLengh < int.MaxValue)
            {
                Assert.Throws<ArgumentException>(() => list.Title = new string('a', maxListTitleLengh + 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create valid title.
        /// </summary>
        [Test]
        public void ToDoList_ValidTitle_Pass()
        {
            ToDoList list = new ToDoList();

            if (minTaskTitleLengh > 0)
            {
                list.Title = new string('a', minTaskTitleLengh);
            }

            list.Title = new string('a', maxTaskTitleLengh);

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create list with null description.
        /// </summary>
        [Test]
        public void ToDoList_NullDescription_ArgumentNullException()
        {
            ToDoList list = new ToDoList();
            Assert.Throws<ArgumentNullException>(() => list.Description = null);
        }

        /// <summary>
        /// Attempt to create list with empty description.
        /// </summary>
        [Test]
        public void ToDoList_EmptyDescription_ArgumentException()
        {
            ToDoList list = new ToDoList();

            Assert.Throws<ArgumentException>(() => list.Description = string.Empty);
        }

        /// <summary>
        /// Attempt to create a description that is too short.
        /// </summary>
        [Test]
        public void ToDoList_ShortDescription_ArgumentException()
        {
            ToDoList list = new ToDoList();

            if (minTaskTitleLengh > 0)
            {
                Assert.Throws<ArgumentException>(() => list.Description = new string('a', minListDescriptionLengh - 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create a description that is too long.
        /// </summary>
        [Test]
        public void ToDoList_LongDescription_ArgumentException()
        {
            ToDoList list = new ToDoList();

            if (maxTaskTitleLengh < int.MaxValue)
            {
                Assert.Throws<ArgumentException>(() => list.Description = new string('a', maxListDescriptionLengh + 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create valid description.
        /// </summary>
        [Test]
        public void ToDoList_ValidDescription_Pass()
        {
            ToDoList list = new ToDoList();

            if (minTaskDescriptionLengh > 0)
            {
                list.Description = new string('a', minTaskDescriptionLengh);
            }

            list.Description = new string('a', maxTaskDescriptionLengh);

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create task with null title.
        /// </summary>
        [Test]
        public void ToDoListTask_NullTitle_ArgumentNullException()
        {
            ToDoListTask task = new ToDoListTask();

            Assert.Throws<ArgumentNullException>(() => task.Title = null);
        }

        /// <summary>
        /// Attempt to create task with empty title.
        /// </summary>
        [Test]
        public void ToDoListTask_EmptyTitle_ArgumentException()
        {
            ToDoListTask task = new ToDoListTask();

            Assert.Throws<ArgumentException>(() => task.Title = string.Empty);
        }

        /// <summary>
        /// Attempt to create a task title that is too short.
        /// </summary>
        [Test]
        public void ToDoListTask_ShortTitle_ArgumentException()
        {
            ToDoListTask task = new ToDoListTask();

            if (minTaskTitleLengh > 0)
            {
                Assert.Throws<ArgumentException>(() => task.Title = new string('a', minTaskTitleLengh - 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create a task title that is too long.
        /// </summary>
        [Test]
        public void ToDoListTask_LongTitle_ArgumentException()
        {
            ToDoListTask task = new ToDoListTask();

            if (maxTaskTitleLengh < int.MaxValue)
            {
                Assert.Throws<ArgumentException>(() => task.Title = new string('a', maxTaskTitleLengh + 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create valid task title.
        /// </summary>
        [Test]
        public void ToDoListTask_ValidTitle_Pass()
        {
            ToDoListTask task = new ToDoListTask();

            if (minTaskTitleLengh > 0)
            {
                task.Title = new string('a', minTaskTitleLengh);
            }

            task.Title = new string('a', maxTaskTitleLengh);

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create task with null description.
        /// </summary>
        [Test]
        public void ToDoListTask_NullDescription_ArgumentNullException()
        {
            ToDoListTask task = new ToDoListTask();

            Assert.Throws<ArgumentNullException>(() => task.Description = null);
        }

        /// <summary>
        /// Attempt to create task with empty description.
        /// </summary>
        [Test]
        public void ToDoListTask_EmptyDescription_ArgumentException()
        {
            ToDoListTask task = new ToDoListTask();

            Assert.Throws<ArgumentException>(() => task.Description = string.Empty);
        }

        /// <summary>
        /// Attempt to create a task description that is too short.
        /// </summary>
        [Test]
        public void ToDoListTask_ShortDescription_ArgumentException()
        {
            ToDoListTask task = new ToDoListTask();

            if (minTaskTitleLengh > 0)
            {
                Assert.Throws<ArgumentException>(() => task.Description = new string('a', minTaskDescriptionLengh - 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create a task description that is too long.
        /// </summary>
        [Test]
        public void ToDoListTask_LongDescription_ArgumentException()
        {
            ToDoListTask task = new ToDoListTask();

            if (maxTaskTitleLengh < int.MaxValue)
            {
                Assert.Throws<ArgumentException>(() => task.Description = new string('a', maxTaskDescriptionLengh + 1));
            }

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create valid task description.
        /// </summary>
        [Test]
        public void ToDoListTask_ValidDescription_Pass()
        {
            ToDoListTask task = new ToDoListTask();

            if (minTaskDescriptionLengh > 0)
            {
                task.Description = new string('a', minTaskDescriptionLengh);
            }

            task.Description = new string('a', maxTaskDescriptionLengh);

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to add a new task if due date in past.
        /// </summary>
        [Test]
        public void ToDoListTask_DueDateInPast_ArgumentException()
        {
            ToDoListTask task = new ToDoListTask();

            Assert.Throws<ArgumentException>(() => task.DueDate = DateTime.Now.AddDays(-5));
        }

        /// <summary>
        /// Attempt to add a new task if due date in future.
        /// </summary>
        [Test]
        public void ToDoListTask_DueDateInFuture_Pass()
        {
            ToDoListTask task = new ToDoListTask();

            task.DueDate = DateTime.Now;

            Assert.Pass();
        }

        /// <summary>
        /// Attempt to create list with null title.
        /// </summary>
        [Test]
        public void ToDoList_NullUserId_ArgumentNullException()
        {
            ToDoList list = new ToDoList();

            Assert.Throws<ArgumentNullException>(() => list.UserId = null);
        }

        /// <summary>
        /// Attempt to create list with empty title.
        /// </summary>
        [Test]
        public void ToDoList_EmptyUserId_ArgumentException()
        {
            ToDoList list = new ToDoList();

            Assert.Throws<ArgumentException>(() => list.UserId = string.Empty);
        }

        /// <summary>
        /// Checking returned tips.
        /// </summary>
        /// <param name="impotant">impotant.</param>
        /// <param name="urgent">urgent.</param>
        /// <returns>returned tip.</returns>
        [TestCase(true, true, ExpectedResult = "tip: DO IT. Task must be completed immediately.")]
        [TestCase(true, false, ExpectedResult = "tip: SCHEDULE IT. the task must be scheduled on your calendar.")]
        [TestCase(false, true, ExpectedResult = "tip: DELEGATE IT. Delegate unimportant tasks to someone else.")]
        [TestCase(false, false, ExpectedResult = "tip: DELETE IT. Cancel the task.")]
        public string EisenhowerMatrix_ImpotantUrgentValues_Tip(bool impotant, bool urgent)
        {
            return EisenhowerMatrix.Tip(impotant, urgent);
        }

        /// <summary>
        /// Check if CustomDateAttribute returned expected value.
        /// </summary>
        /// <param name="addDay">addDay.</param>
        /// <returns>CustomDateAttribute value.</returns>
        [TestCase(1, ExpectedResult = true)]
        [TestCase(-1, ExpectedResult = false)]
        public bool CustomDateAttribute_Values_ReturnedValues(int addDay)
        {
            CustomDateAttribute customDateAttribute = new CustomDateAttribute();
            return customDateAttribute.IsValid(DateTime.Now.AddDays(addDay));
        }

        /// <summary>
        /// Whether the CustomDateAttribute will return false if passed a null parameter.
        /// </summary>
        [Test]
        public void CustomDateAttribute_null_false()
        {
            CustomDateAttribute customDateAttribute = new CustomDateAttribute();
            Assert.IsFalse(customDateAttribute.IsValid(null));
        }
    }
}
