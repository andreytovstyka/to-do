﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using NUnit.Framework;
using Todo_aspnetmvc_ui.Controllers;
using Todo_aspnetmvc_ui.Models.ViewModels;
using Todo_aspnetmvc_ui.ViewComponents;
using Todo_data.RepositoryMock;
using Todo_services;

namespace UI.ViewComponents.Tests
{
    public class RemainderViewComponentTests
    {
        private Mock<UserManager<IdentityUser>> userManager;
        private ToDoListSevices toDoListSevices;
        private ToDoListTaskSevices toDoListTaskSevices;
        private ToDoListRepositoryMock listRepository;
        private ToDoListTaskRepositoryMock taskRepository;
        private RemainderViewComponent component;

        /// <summary>
        /// Initialization of mock data. Creating a new controller instance.
        /// </summary>
        [SetUp]
        public void RunBeforeTests()
        {
            this.listRepository = new ToDoListRepositoryMock();
            this.taskRepository = new ToDoListTaskRepositoryMock();
            this.toDoListSevices = new ToDoListSevices(this.listRepository);
            this.toDoListTaskSevices = new ToDoListTaskSevices(this.taskRepository);

            this.userManager = new Mock<UserManager<IdentityUser>>(new Mock<IUserStore<IdentityUser>>().Object, null, null, null, null, null, null, null, null);

            this.component = new RemainderViewComponent(this.userManager.Object, this.toDoListSevices, this.toDoListTaskSevices);
        }

        /// <summary>
        /// Check if the number of reminders is as expected.
        /// </summary>
        /// <returns>Number of reminders.</returns>
        [Test]
        public async Task InvokeAsync_invoke_ReturnedModel()
        {
            var expected = 1;
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            this.userManager.Setup(x => x.GetUserId(It.IsAny<ClaimsPrincipal>()))
                       .Returns("test");

            var act = await this.component.InvokeAsync() as ViewResult;

            Assert.That(this.component.ViewBag.reminders, Is.EqualTo(expected));
        }
    }
}
