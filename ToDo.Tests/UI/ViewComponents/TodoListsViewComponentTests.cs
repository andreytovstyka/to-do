﻿using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Moq;
using NUnit.Framework;
using Todo_aspnetmvc_ui.ViewComponents;
using Todo_data.RepositoryMock;
using Todo_services;

namespace UI.ViewComponents.Tests
{
    public class TodoListsViewComponentTests
    {
        private Mock<UserManager<IdentityUser>> userManager;
        private ToDoListSevices toDoListSevices;
        private ToDoListRepositoryMock listRepository;
        private ToDoListTaskRepositoryMock taskRepository;
        private TodoListsViewComponent component;

        /// <summary>
        /// Initialization of mock data. Creating a new controller instance.
        /// </summary>
        [SetUp]
        public void RunBeforeTests()
        {
            this.listRepository = new ToDoListRepositoryMock();
            this.taskRepository = new ToDoListTaskRepositoryMock();
            this.toDoListSevices = new ToDoListSevices(this.listRepository);

            this.userManager = new Mock<UserManager<IdentityUser>>(new Mock<IUserStore<IdentityUser>>().Object, null, null, null, null, null, null, null, null);

            this.component = new TodoListsViewComponent(this.userManager.Object, this.toDoListSevices);
        }

        /// <summary>
        /// Check if the list todo list is as expected.
        /// </summary>
        /// <returns>Lists.</returns>
        [Test]
        public async Task InvokeAsync_invoke_Lists()
        {
            var expected = this.listRepository.GetAllAsync();
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            this.userManager.Setup(x => x.GetUserId(It.IsAny<ClaimsPrincipal>()))
                       .Returns("test");

            var act = await this.component.InvokeAsync() as ViewViewComponentResult;

            Assert.That(expected.Result, Is.EqualTo(act.ViewData.Model));
        }
    }
}
