﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Todo_aspnetmvc_ui.Controllers;
using Todo_domain_entities;

namespace UI.Controllers.Tests
{
    /// <summary>
    /// HomeC ontroller Tests.
    /// </summary>
    public class HomeControllerTests
    {
        private HomeController controller;

        /// <summary>
        /// Run before every tests. Creating a new controller instence.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(
                new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "test"),
            }, "mock"));

            this.controller = new HomeController(new NullLogger<HomeController>());

            this.controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user },
            };
        }

        /// <summary>
        /// Is HomeController/Index returns view.
        /// </summary>
        [Test]
        public void Index_ReturnsView()
        {
            var act = this.controller.Index() as ViewResult;

            Assert.That(act, Is.Not.Null);
        }

        /// <summary>
        /// Is HomeController/Error returns right messages.
        /// </summary>
        /// <param name="errorStatusCode">error status code.</param>
        /// <returns>result.</returns>
        [TestCase(404, ExpectedResult = "Page not found.")]
        [TestCase(499, ExpectedResult = "Client Error.")]
        [TestCase(500, ExpectedResult = "Server Error.")]
        [TestCase(777, ExpectedResult = "Unknown Error.")]
        public string Error_ErrorStatusCode_Message(int errorStatusCode)
        {
            ViewResult act = this.controller.Error(errorStatusCode) as ViewResult;

            return this.controller.ViewBag.message;
        }
    }
}
