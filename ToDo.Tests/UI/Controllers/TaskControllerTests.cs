﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Todo_aspnetmvc_ui.Controllers;
using Todo_aspnetmvc_ui.Models.ViewModels;
using Todo_data.RepositoryMock;
using Todo_domain_entities;
using Todo_services;

namespace UI.Controllers.Tests
{
    /// <summary>
    /// TaskController Tests.
    /// </summary>
    public class TaskControllerTests
    {
        private ToDoListSevices toDoListSevices;
        private ToDoListTaskSevices toDoTaskSevices;
        private ToDoListRepositoryMock listRepository;
        private ToDoListTaskRepositoryMock taskRepository;
        private TaskController controller;

        private static IEnumerable<TestCaseData> DataCasesForRemindersTest
        {
            get
            {
                yield return new TestCaseData(
                    new long[] { 2, 3, 4, 7, 8 });
            }
        }

        private static IEnumerable<TestCaseData> DataCasesForFilterTest
        {
            get
            {
                yield return new TestCaseData(
                    "today",
                    new long[] { 1 });
                yield return new TestCaseData(
                    "planned",
                    new long[] { 2, 3, 5, 7, 8 });
                yield return new TestCaseData(
                    "overdue",
                    new long[] { });
            }
        }

        /// <summary>
        /// Initialization of mock data. Creating a new controller instance.
        /// </summary>
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            this.listRepository = new ToDoListRepositoryMock();
            this.taskRepository = new ToDoListTaskRepositoryMock();
            this.toDoListSevices = new ToDoListSevices(this.listRepository);
            this.toDoTaskSevices = new ToDoListTaskSevices(this.taskRepository);

            var user = new ClaimsPrincipal(new ClaimsIdentity(
    new Claim[]
{
                new Claim(ClaimTypes.NameIdentifier, "test"),
}, "mock"));

            this.controller = new TaskController(this.toDoTaskSevices, this.toDoListSevices, new NullLogger<TaskController>());

            this.controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user },
            };
        }

        /// <summary>
        /// Run before every tests.
        /// </summary>
        /// <returns>Result of task.</returns>
        [SetUp]
        public async Task Setup()
        {
            this.listRepository.Lists.Clear();
            this.taskRepository.Tasks.Clear();
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");
        }

        /// <summary>
        /// Is TaskController/Tasks returns list of tasks.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_Tasks_ListOfTasks()
        {
            var tasks = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1).ToDoListTasks;

            var act = await this.controller.Tasks(1) as ViewResult;

            Assert.That(tasks, Is.EqualTo(((TasksViewModel)act.ViewData.Model).TodoTasks));
        }

        /// <summary>
        /// Is TaskController/Filter returns list of today/planned/overdue tasks.
        /// </summary>
        /// <param name = "filter" > filter.</ param >
        /// <param name = "expected" > expected.</ param >
        /// <returns>Result of checking.</returns>
        [TestCaseSource(nameof(DataCasesForFilterTest))]
        public async Task TaskController_Filter_ListOfTasks(string filter, IEnumerable<long> expected)
        {
            var act = await this.controller.Filter(filter) as ViewResult;

            Assert.That(expected, Is.EqualTo(((TasksViewModel)act.ViewData.Model).TodoTasks.Select(x => x.Id)));
        }

        /// <summary>
        /// Is TaskController/Filter returns list of reminders tasks.
        /// </summary>
        /// <param name = "expected" > expected.</ param >
        /// <returns>Result of checking.</returns>
        [TestCaseSource(nameof(DataCasesForRemindersTest))]
        public async Task TaskController_Reminders_ListOfTasks(IEnumerable<long> expected)
        {
            foreach (var item in expected)
            {
                var arr = (await this.taskRepository.GetAllAsync()).FirstOrDefault(x => x.Id == item);
                arr.Remind = DateTime.Now.AddDays(-1 - item);
            }

            var act = await this.controller.Reminders() as ViewResult;

            Assert.That(expected, Is.EqualTo(((TasksViewModel)act.ViewData.Model).TodoTasks.Select(x => x.Id)));
        }

        /// <summary>
        /// Whether the task has been created and redirects the user to the list view page.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_CreatePost_CreatedTask()
        {
            ToDoListTask toDoListTask = new ToDoListTask() { Description = "Test Description", Title = "Test Title", ToDoListId = 1 };

            var act = (RedirectToActionResult)await this.controller.Create(1, toDoListTask, "returnUrl");

            var actTask = (await this.taskRepository.GetAllAsync()).FirstOrDefault(x => x.Title == "Test Title");
            Assert.Multiple(() =>
            {
                Assert.That(actTask.Description, Is.EqualTo("Test Description"));
                Assert.That(act.ActionName, Is.EqualTo("Tasks"));
                Assert.That(act.ControllerName, Is.EqualTo("Task"));
                Assert.That(act.RouteValues.FirstOrDefault(x => x.Key == "id").Value.ToString(), Is.EqualTo("1"));
            });
        }

        /// <summary>
        /// Is TaskController/Create returns view.
        /// </summary>
        [Test]
        public void TaskController_CreateGet_View()
        {
            var act = this.controller.Create(0) as ViewResult;

            Assert.That(act, Is.Not.Null);
        }

        /// <summary>
        /// Is TaskController/Edit returns view.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_EditGet_View()
        {
            var act = await this.controller.Edit(1) as ViewResult;
            var mod = (TaskDataInputViewModel)act.ViewData.Model;
            Assert.Multiple(() =>
            {
                Assert.That(act, Is.Not.Null);
                Assert.That(mod.ToDoTask.Id, Is.EqualTo(1));
                Assert.That(mod.ToDoTask.Title, Is.EqualTo("Budget planning"));
                Assert.That(mod.ToDoTask.Description, Is.EqualTo("next month"));
            });
        }

        /// <summary>
        /// Whether the task has been edited and redirects the user to the preview page.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_EditPost_EditedList()
        {
            ToDoListTask toDoTask = new ToDoListTask() { Description = "Test Description", Title = "Test Title", ToDoListId = 1, Id = 1 };

            var act = (RedirectResult)await this.controller.Edit(toDoTask, "returnUrl");

            var actTask = (await this.taskRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);
            Assert.Multiple(() =>
            {
                Assert.That(actTask.Title, Is.EqualTo("Test Title"));
                Assert.That(actTask.Description, Is.EqualTo("Test Description"));
                Assert.That(act.Url, Is.EqualTo("returnUrl"));
            });
        }

        /// <summary>
        ///  Whether the task status has been changed.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_ChangeStatus_StatusChanged()
        {
            await this.controller.ChangeStatus(1, 1, "2");

            var actTask = (await this.taskRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);

            Assert.That(actTask.TaskStatus, Is.EqualTo(Status.Completed));
        }

        /// <summary>
        /// Is TaskController/Delete returns view.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_DeleteGet_View()
        {
            var act = await this.controller.Delete(1) as ViewResult;
            var mod = (TaskDataInputViewModel)act.ViewData.Model;
            Assert.Multiple(() =>
            {
                Assert.That(act, Is.Not.Null);
                Assert.That(mod.ToDoTask.Id, Is.EqualTo(1));
                Assert.That(mod.ToDoTask.Title, Is.EqualTo("Budget planning"));
                Assert.That(mod.ToDoTask.Description, Is.EqualTo("next month"));
            });
        }

        /// <summary>
        /// Whether the task has been deleted and redirects the user to the main page.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_DeletePost_ListDeleted()
        {
            var arr = (await this.taskRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 0);

            var act = (RedirectResult)await this.controller.Delete(arr, "returnUrl");

            Assert.IsFalse((await this.taskRepository.GetAllAsync()).Any(x => x.Id == 0));
            Assert.That(act.Url, Is.EqualTo("returnUrl"));
        }

        /// <summary>
        ///  Whether the task reminder has been set.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task TaskController_SetReminder_ReminderSet()
        {
            var actTask = (await this.taskRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);

            var act = (RedirectResult)await this.controller.SetReminder(1, 1, DateTime.Today.AddHours(1));

            Assert.That(actTask.Remind, Is.EqualTo(DateTime.Today.AddHours(1)));
        }
    }
}
