﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Todo_aspnetmvc_ui.Controllers;
using Todo_aspnetmvc_ui.Models.ViewModels;
using Todo_data.RepositoryMock;
using Todo_domain_entities;
using Todo_services;

namespace UI.Controllers.Tests
{
    /// <summary>
    /// ListController Tests.
    /// </summary>
    public class ListControllerTests
    {
        private ToDoListSevices toDoListSevices;
        private ToDoListRepositoryMock listRepository;
        private ToDoListTaskRepositoryMock taskRepository;
        private ListController controller;
        private ListController controllerErrorGen;

        /// <summary>
        /// Initialization of mock data. Creating a new controller instance.
        /// </summary>
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            this.listRepository = new ToDoListRepositoryMock();
            this.taskRepository = new ToDoListTaskRepositoryMock();
            this.toDoListSevices = new ToDoListSevices(this.listRepository);

            var user = new ClaimsPrincipal(new ClaimsIdentity(
    new Claim[]
{
                new Claim(ClaimTypes.NameIdentifier, "test"),
}, "mock"));

            this.controller = new ListController(this.toDoListSevices, new NullLogger<ListController>());
            this.controllerErrorGen = new ListController(new ListServicesErrorGen(), new NullLogger<ListController>());

            this.controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user },
            };

            this.controllerErrorGen.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user },
            };
        }

        /// <summary>
        /// Run before every tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.listRepository.Lists.Clear();
            this.taskRepository.Tasks.Clear();
        }

        /// <summary>
        /// Whether the to-do list has been created and redirects the user to the list view page.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task CreatePost_ValidModelState_CreatedList()
        {
            ToDoList toDoList = new ToDoList() { Description = "Test Description", Hide = false, Title = "Title Test" };

            this.controller.ModelState.Clear();

            var act = (RedirectToActionResult)await this.controller.Create(toDoList);

            var actList = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Title == "Title Test");
            Assert.Multiple(() =>
            {
                Assert.That(actList.Description, Is.EqualTo("Test Description"));
                Assert.That(act.ActionName, Is.EqualTo("Tasks"));
                Assert.That(act.ControllerName, Is.EqualTo("Task"));
            });
        }

        /// <summary>
        /// Whether the todolist view was returned if the model was invalid.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task CreatePost_InvalidModelState_View()
        {
            this.controller.ModelState.AddModelError("key", "error message");

            var act = (ViewResult)await this.controller.Create(new ToDoList());

            Assert.That(act.Model.GetType().Name, Is.EqualTo("ToDoList"));
        }

        /// <summary>
        /// Whether the error page was returned if the exception was occured in AddListAsync method.
        /// </summary>
        /// <returns>Error page.</returns>
        [Test]
        public async Task CreatePost_AddListAsyncMethodException_ErrorPage()
        {
            var act = (RedirectResult)await this.controllerErrorGen.Create(new ToDoList());

            Assert.That(act.Url, Is.EqualTo("/Error.html"));
        }

        /// <summary>
        /// Is ListController/Create returns view.
        /// </summary>
        [Test]
        public void CreateGet_Invoke_View()
        {
            var act = this.controller.Create() as ViewResult;

            Assert.That(act, Is.Not.Null);
        }

        /// <summary>
        /// Is ListController/Edit returns view.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task EditGet_ListId_View()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            this.controller.HttpContext.Request.Headers["Referer"] = "/";

            var act = await this.controller.Edit(1) as ViewResult;
            var mod = (ListDataInputViewModel)act.ViewData.Model;
            Assert.Multiple(() =>
            {
                Assert.That(act, Is.Not.Null);
                Assert.That(mod.TodoList.Id, Is.EqualTo(1));
                Assert.That(mod.TodoList.Title, Is.EqualTo("Home"));
                Assert.That(mod.TodoList.Description, Is.EqualTo("home & family"));
            });
        }

        /// <summary>
        /// Whether the to-do list has been edited and redirects the user to the preview page.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task EditPost_List_EditedList()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            this.controller.ModelState.Clear();

            ToDoList toDoList = new ToDoList() { Description = "Test Description", Hide = false, Title = "Test Title", Id = 1 };

            var act = (RedirectResult)await this.controller.Edit(1, toDoList, "returnUrl");

            var actList = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);
            Assert.Multiple(() =>
            {
                Assert.That(actList.Description, Is.EqualTo("Test Description"));
                Assert.That(actList.Title, Is.EqualTo("Test Title"));
                Assert.That(act.Url, Is.EqualTo("returnUrl"));
            });
        }

        /// <summary>
        /// Whether the todolist view was returned if the model was invalid.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task EditPost_InvalidModelState_View()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            this.controller.ModelState.AddModelError("key", "error message");

            var act = (ViewResult)await this.controller.Edit(1, new ToDoList(), "returnUrl");

            Assert.That(act.Model.GetType().Name, Is.EqualTo("ToDoList"));
        }

        /// <summary>
        /// Whether the error page was returned if the exception was occured in UpdateListAsync method.
        /// </summary>
        /// <returns>Error page.</returns>
        [Test]
        public async Task EditPost_UpdateListAsyncMethodException_ErrorPage()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var act = (RedirectResult)await this.controllerErrorGen.Edit(1, new ToDoList(), "returnUrl");

            Assert.That(act.Url, Is.EqualTo("/Error.html"));
        }

        /// <summary>
        ///  Whether the to-do list visibility has been changed.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task ChangeVisibility_ListId_VisibilityChanged()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            await this.controller.ChangeVisibility(1);

            var actList = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);

            Assert.IsTrue(actList.Hide);
        }

        /// <summary>
        /// Whether the error page was returned if the exception was occured in ChangeVisibilityAsync method.
        /// </summary>
        /// <returns>Error page.</returns>
        [Test]
        public async Task ChangeVisibility_ChangeVisibilityAsyncMethodException_ErrorPage()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var act = (RedirectResult)await this.controllerErrorGen.ChangeVisibility(1);

            Assert.That(act.Url, Is.EqualTo("/Error.html"));
        }

        /// <summary>
        ///  Whether the to-do list has been copied.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task Copy_ListId_ListCopied()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            ToDoList arr = this.listRepository.Lists[0];

            var actId = await this.controller.Copy(arr.Id);

            var actList = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == arr.Id);

            List<ToDoListTask> arrTasks = arr.ToDoListTasks.OrderBy(x => x.Title).ToList();
            List<ToDoListTask> actTasks = actList.ToDoListTasks.OrderBy(x => x.Title).ToList();

            Assert.Multiple(() =>
            {
                Assert.That(arr.Title, Is.EqualTo(actList.Title));
                Assert.That(arr.Description, Is.EqualTo(actList.Description));
                Assert.That(arr.Hide, Is.EqualTo(actList.Hide));
            });
            for (int i = 0; i < arrTasks.Count; i++)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(arrTasks[i].Id, Is.EqualTo(actTasks[i].Id));
                    Assert.That(arrTasks[i].Description, Is.EqualTo(actTasks[i].Description));
                    Assert.That(arrTasks[i].DueDate, Is.EqualTo(actTasks[i].DueDate));
                    Assert.That(arrTasks[i].Important, Is.EqualTo(actTasks[i].Important));
                    Assert.That(arrTasks[i].Remind, Is.EqualTo(actTasks[i].Remind));
                    Assert.That(arrTasks[i].TaskStatus, Is.EqualTo(actTasks[i].TaskStatus));
                    Assert.That(arrTasks[i].Title, Is.EqualTo(actTasks[i].Title));
                    Assert.That(arrTasks[i].Urgent, Is.EqualTo(actTasks[i].Urgent));
                });
            }
        }

        /// <summary>
        /// Whether the error page was returned if the exception was occured in CopyListAsync method.
        /// </summary>
        /// <returns>Error page.</returns>
        [Test]
        public async Task Copy_CopyAsyncMethodException_ErrorPage()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var act = (RedirectResult)await this.controllerErrorGen.Copy(1);

            Assert.That(act.Url, Is.EqualTo("/Error.html"));
        }

        /// <summary>
        /// Is ListController/Delete returns view.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task DeleteGet_ListId_View()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var act = await this.controller.Delete(1) as ViewResult;
            var mod = (ListDataInputViewModel)act.ViewData.Model;
            Assert.Multiple(() =>
            {
                Assert.That(act, Is.Not.Null);
                Assert.That(mod.TodoList.Id, Is.EqualTo(1));
                Assert.That(mod.TodoList.Title, Is.EqualTo("Home"));
                Assert.That(mod.TodoList.Description, Is.EqualTo("home & family"));
            });
        }

        /// <summary>
        /// Whether the to-do list has been deleted and redirects the user to the main page.
        /// </summary>
        /// <returns>Result of checking.</returns>
        [Test]
        public async Task DeletePost_List_ListDeleted()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var arr = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);

            var act = (RedirectToActionResult)await this.controller.Delete(arr);

            Assert.IsFalse((await this.listRepository.GetAllAsync()).Any(x => x.Id == 1));
            Assert.That(act.ActionName, Is.EqualTo("Index"));
            Assert.That(act.ControllerName, Is.EqualTo("Home"));
        }

        /// <summary>
        /// Whether the error page was returned if the exception was occured in DeleteListAsync method.
        /// </summary>
        /// <returns>Error page.</returns>
        [Test]
        public async Task Delete_DeleteAsyncMethodException_ErrorPage()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var arr = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);

            var act = (RedirectResult)await this.controllerErrorGen.Delete(arr);

            Assert.That(act.Url, Is.EqualTo("/Error.html"));
        }

        /// <summary>
        /// An attempt was made to access another user's data.
        /// </summary>
        /// <returns>Exception.</returns>
        [Test]
        public async Task Delete_AnotherUser_ErrorPage()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var arr = (await this.listRepository.GetAllAsync()).FirstOrDefault(x => x.Id == 1);
            arr.UserId = "another user";

            Assert.ThrowsAsync<InvalidOperationException>(async () => await this.controller.Delete(arr));
        }
    }
}
