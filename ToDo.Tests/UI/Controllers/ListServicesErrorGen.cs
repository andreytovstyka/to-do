﻿using Todo_domain_entities;
using Todo_services.Interfaces;

namespace UI.Controllers.Tests
{
    internal class ListServicesErrorGen : IToDoListSevices
    {
        public Task<long> AddListAsync(ToDoList toDoList)
        {
            throw new NotImplementedException();
        }

        public Task ChangeVisibilityAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<long> CopyListAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task DeleteListAsync(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<ToDoList>> GetAllListsAsync()
        {
            return await Task.Run(() =>
            {
                var result = new List<ToDoList>();
                ToDoList list = new()
                {
                    UserId = "test",
                    Id = 1,
                };

                result.Add(list);

                return result.AsQueryable();
            });
        }

        public Task<bool> IsToDoListExistsAsync(long idToDoList)
        {
            throw new NotImplementedException();
        }

        public Task UpdateListAsync(ToDoList toDoList)
        {
            throw new NotImplementedException();
        }
    }
}
