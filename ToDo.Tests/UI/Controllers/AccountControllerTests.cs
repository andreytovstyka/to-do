﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using NUnit.Framework;
using Todo_aspnetmvc_ui.Controllers;
using Todo_aspnetmvc_ui.Models.ViewModels;

namespace UI.Controllers.Tests
{
    public class AccountControllerTests
    {
        private Mock<UserManager<IdentityUser>> userManager;
        private Mock<SignInManager<IdentityUser>> signInManager;
        private Mock<IServiceProvider> serviceProvider;
        private AccountController controller;

        /// <summary>
        /// Initialization of mock data. Creating a new controller instance.
        /// </summary>
        [SetUp]
        public void RunBeforeTests()
        {
            this.userManager = new Mock<UserManager<IdentityUser>>(new Mock<IUserStore<IdentityUser>>().Object, null, null, null, null, null, null, null, null);
            this.signInManager = new Mock<SignInManager<IdentityUser>>(this.userManager.Object, new Mock<IHttpContextAccessor>().Object, new Mock<IUserClaimsPrincipalFactory<IdentityUser>>().Object, null, null, null, null);
            this.serviceProvider = new Mock<IServiceProvider>();

            var user = new ClaimsPrincipal(new ClaimsIdentity(
new Claim[]
{
                new Claim(ClaimTypes.NameIdentifier, "test"),
}, "mock"));

            this.controller = new AccountController(this.userManager.Object, this.signInManager.Object, this.serviceProvider.Object, new NullLogger<ListController>());

            this.controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user },
            };
        }

        [Test]
        public void LoginGet_invoke_ReturnedModel()
        {
            var act = this.controller.Login() as ViewResult;

            Assert.That(act.Model.GetType().Name, Is.EqualTo("LoginViewModel"));
        }

        [Test]
        public async Task LoginPost_InvalidModel_CurrentView()
        {
            var loginData = new LoginViewModel { EmailAddress = "testtest.com", Password = string.Empty, RememberMe = false };

            this.controller.ModelState.AddModelError("key", "error message");

            var act = await this.controller.Login(loginData) as ViewResult;

            Assert.That(act, Is.Not.Null);
        }

        [Test]
        public async Task LoginPost_InvalidLogin_ErrorMessageIsShown()
        {
            var loginData = new LoginViewModel { EmailAddress = "test@test.com", Password = "testPassword", RememberMe = false };

            this.signInManager.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(Microsoft.AspNetCore.Identity.SignInResult.Failed));

            var act = await this.controller.Login(loginData);

            Assert.That(this.controller.ViewData.ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage, Is.EqualTo("Login error!"));
            Assert.That((ViewResult)act, Is.Not.Null);
        }

        [TestCase(null, "/")]
        [TestCase("Index", "Index")]
        public async Task LoginPost_ValidLogin_RiderectTo(string returnUrl, string expect)
        {
            var loginData = new LoginViewModel { EmailAddress = "test@test.com", Password = "testPassword", RememberMe = false, ReturnUrl = returnUrl };

            this.signInManager.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(Microsoft.AspNetCore.Identity.SignInResult.Success));

            var act = await this.controller.Login(loginData) as RedirectResult;

            Assert.That(act.Url, Is.EqualTo(expect));
        }

        [TestCase(null, "/")]
        [TestCase("testUrl", "test")]
        public async Task LogOut_NullReferer_RiderectTo(string returnUrl, string expectedUrl)
        {
            if (returnUrl is not null)
            {
                this.controller.HttpContext.Request.Headers["Referer"] = "testUrl";
                this.controller.HttpContext.Request.Headers["Origin"] = "Url";
            }

            var act = await this.controller.Logout() as RedirectToActionResult;

            Assert.That(act.ActionName, Is.EqualTo("Login"));
            Assert.That(act.ControllerName, Is.EqualTo("Account"));
            Assert.That(act.RouteValues.FirstOrDefault(x => x.Key == "ReturnUrl").Value, Is.EqualTo(expectedUrl));
        }

        [Test]
        public void RegisterGet_Invoke_ReturnedModel()
        {
            var act = this.controller.Register() as ViewResult;

            Assert.That(act.Model.GetType().Name, Is.EqualTo("RegisterViewModel"));
        }

        [Test]
        public async Task RegisterPost_InvalidModel_CurrentView()
        {
            var loginData = new RegisterViewModel { EmailAddress = "test@test.com", Password = "testPassword", ConfirmPassword = "Password" };

            this.controller.ModelState.AddModelError("key", "error message");

            var act = await this.controller.Register(loginData) as ViewResult;

            Assert.That(act, Is.Not.Null);
        }

        [Test]
        public async Task RegisterPost_PasswordMismatch_ViewRetutn()
        {
            this.userManager.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                       .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError[] { new IdentityError { Code = "000", Description = "error" } })));

            var loginData = new RegisterViewModel { EmailAddress = "test@test.com", Password = "testPassword", ConfirmPassword = "Password" };

            var act = await this.controller.Register(loginData);

            Assert.That((ViewResult)act, Is.Not.Null);
        }

        [Test]
        public async Task RegisterPost_Data_ReturnLoginPage()
        {
            var dummyUser = new IdentityUser() { UserName = "test@test.com", Email = "test@test.com" };

            this.userManager.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                       .Returns(Task.FromResult(IdentityResult.Success));

            this.userManager.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
           .Returns(Task.FromResult(dummyUser));

            var loginData = new RegisterViewModel { EmailAddress = "test@test.com", Password = "testPassword", ConfirmPassword = "testPassword" };

            var act = await this.controller.Register(loginData) as RedirectToActionResult;

            Assert.That(act.ActionName, Is.EqualTo("Login"));
        }
    }
}
