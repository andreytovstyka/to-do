﻿using NUnit.Framework;
using Todo_data.RepositoryMock;
using Todo_domain_entities;
using Todo_services;

namespace Services.Tests
{
    /// <summary>
    /// Services Tests.
    /// </summary>
    public class ServicesTests
    {
        private ToDoListSevices toDoListSevices;
        private ToDoListTaskSevices toDoTaskSevices;
        private ToDoListRepositoryMock listRepository;
        private ToDoListTaskRepositoryMock taskRepository;

        /// <summary>
        /// Initialization of mock data.
        /// </summary>
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            this.listRepository = new ToDoListRepositoryMock();
            this.taskRepository = new ToDoListTaskRepositoryMock();
            this.toDoListSevices = new ToDoListSevices(this.listRepository);
            this.toDoTaskSevices = new ToDoListTaskSevices(this.taskRepository);
        }

        /// <summary>
        /// Run before every tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.listRepository.Lists.Clear();
            this.taskRepository.Tasks.Clear();
        }

        /// <summary>
        /// Is there an existing identifier.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task IsToDoListExists_ExistingId_true()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            bool act = await this.toDoListSevices.IsToDoListExistsAsync(1);

            Assert.That(act, Is.True);
        }

        /// <summary>
        /// Attempt pass null to ToDoListSevices constructor.
        /// </summary>
        [Test]
        public void ToDoListSevices_PassNullToConstructor_ArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new ToDoListSevices(null));
        }

        /// <summary>
        /// Attempt pass null to ToDoListTaskSevices constructor.
        /// </summary>
        [Test]
        public void ToDoListTaskSevices_PassNullToConstructor_ArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new ToDoListTaskSevices(null));
        }

        [Test]
        public async Task CopyListAsync_PassUnrealId_MinusOne()
        {
            var act = await this.toDoListSevices.CopyListAsync(-2);
            Assert.That(act, Is.EqualTo(-1));
        }

        /// <summary>
        /// Is there an existing identifier.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task IsToDoListExists_NonExistingId_false()
        {
            bool act = await this.toDoListSevices.IsToDoListExistsAsync(100);

            Assert.That(act, Is.False);
        }

        /// <summary>
        /// The test of adding a new list.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task AddList_CreatedList_NewListAdded()
        {
            ToDoList toDoList = new ToDoList
            {
                Title = "New list",
                Description = "The test of adding a new list.",
            };

            await this.toDoListSevices.AddListAsync(toDoList);

            var act = (await this.toDoListSevices.GetAllListsAsync()).Any(x => x.Title == "New list");

            Assert.That(act, Is.True);
        }

        /// <summary>
        /// Attempt pas null.
        /// </summary>
        [Test]
        public void AddList_NulldList_ArgumentNullException()
        {
            ToDoList toDoList = null;

            Assert.ThrowsAsync<ArgumentNullException>(async () => await this.toDoListSevices.AddListAsync(toDoList));
        }

        /// <summary>
        /// The test of updating a list.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task UpdateList_UpdatedList_ListUpdated()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            ToDoList list = (await this.toDoListSevices.GetAllListsAsync()).FirstOrDefault(x => x.Id == 1);
            list.Title = "Updated title";
            await this.toDoListSevices.UpdateListAsync(list);

            var act = (await this.toDoListSevices.GetAllListsAsync()).FirstOrDefault(x => x.Id == 1).Title;

            Assert.That(act, Is.EqualTo("Updated title"));
        }

        /// <summary>
        /// Is there an existing identifier.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task IsTaskExists_ExistingId_true()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            bool act = await this.toDoTaskSevices.IsTaskExistsAsync(1);

            Assert.That(act, Is.True);
        }

        /// <summary>
        /// Is there an existing identifier.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task IsTaskExists_NonExistingId_false()
        {
            bool act = await this.toDoTaskSevices.IsTaskExistsAsync(100);

            Assert.That(act, Is.False);
        }

        /// <summary>
        /// The test of adding a new task.
        /// </summary>
        /// <param name="title">title.</param>
        /// <param name="expectedTitle">expected Title.</param>
        /// <returns>representing the asynchronous unit test.</returns>
        [TestCase("New task", "New task")]
        [TestCase("Budget planning", "Budget planning(1)")]
        [TestCase("Thirty characters long title", "Thirty characters long title(1")]
        public async Task AddTask_CreatedList_NewListAdded(string title, string expectedTitle)
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            if (title == "Thirty characters long title")
            {
                (await this.toDoTaskSevices.GetAllTasksAsync()).FirstOrDefault(x => x.ToDoListId == 1).Title = title;
            }

            ToDoListTask toDoTask = new ToDoListTask
            {
                Title = title,
                Description = "The test of adding a new task.",
                DueDate = DateTime.Now.AddDays(1),
                TaskStatus = Status.Created,
                ToDoListId = 1,
            };

            await this.toDoTaskSevices.AddTaskAsync(toDoTask);

            var act = (await this.toDoTaskSevices.GetAllTasksAsync()).Any(x => x.Title == expectedTitle);

            Assert.That(act, Is.True);
        }

        /// <summary>
        /// Attempt pass null.
        /// </summary>
        [Test]
        public void AddTask_NullTask_ArgumentNullException()
        {
            ToDoListTask toDoTask = null;

            Assert.ThrowsAsync<ArgumentNullException>(async () => await this.toDoTaskSevices.AddTaskAsync(toDoTask));
        }

        /// <summary>
        /// The test of updating a task.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task UpdateTask_UpdatedList_ListUpdated()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            ToDoListTask intTask = (await this.toDoTaskSevices.GetAllTasksAsync()).FirstOrDefault();
            ToDoListTask updatingTask = new ToDoListTask()
            {
                Description = "New description",
                DueDate = intTask.DueDate.AddDays(1),
                Id = intTask.Id,
                Important = !intTask.Important,
                Remind = (intTask.Remind ?? DateTime.MinValue).AddDays(1),
                TaskStatus = intTask.TaskStatus == Status.Completed ? Status.Started : (Status)((byte)intTask.TaskStatus + 1),
                Title = "New Title",
                ToDoListId = intTask.ToDoListId,
                Urgent = !intTask.Urgent,
            };

            await this.toDoTaskSevices.UpdateTaskAsync(updatingTask);

            var updatedTask = (await this.toDoTaskSevices.GetAllTasksAsync()).FirstOrDefault(x => x.Id == intTask.Id);

            Assert.That(updatedTask.Id, Is.EqualTo(updatingTask.Id));
            Assert.That(updatedTask.Important, Is.EqualTo(updatingTask.Important));
            Assert.That(updatedTask.Description, Is.EqualTo(updatingTask.Description));
            Assert.That(updatedTask.DueDate, Is.EqualTo(updatingTask.DueDate));
            Assert.That(updatedTask.Remind, Is.EqualTo(updatingTask.Remind));
            Assert.That(updatedTask.TaskStatus, Is.EqualTo(updatingTask.TaskStatus));
            Assert.That(updatedTask.Title, Is.EqualTo(updatingTask.Title));
            Assert.That(updatedTask.ToDoListId, Is.EqualTo(updatingTask.ToDoListId));
            Assert.That(updatedTask.Urgent, Is.EqualTo(updatingTask.Urgent));
        }

        /// <summary>
        /// Attempt pass null.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task UpdateTask_NullTask_ArgumentNullException()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            ToDoListTask toDoTask = null;

            Assert.ThrowsAsync<ArgumentNullException>(() => this.toDoTaskSevices.UpdateTaskAsync(toDoTask));
        }

        /// <summary>
        /// The test of updating status.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task ChangeStatus_Completed_Done()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            await this.toDoTaskSevices.ChangeStatusAsync(1, Status.Completed);

            var act = (await this.toDoTaskSevices.GetAllTasksAsync()).FirstOrDefault(x => x.Id == 1).TaskStatus;

            Assert.That(act, Is.EqualTo(Status.Completed));
        }

        /// <summary>
        /// Test for receiving all tasks.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetAllTasks_Tasks_ReturnsDate()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            List<ToDoListTask> expectedTasks = new List<ToDoListTask>
            {
                new ToDoListTask
                {
                    Id = 1,
                    ToDoListId = 1,
                    Title = "Budget planning",
                    Description = "next month",
                    DueDate = DateTime.Now.Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Id = 2,
                    ToDoListId = 1,
                    Title = "Buy a climbing frame for a cat",
                    Description = "with a place to sleep",
                    DueDate = DateTime.Now.AddDays(2).Date,
                    Important = true,
                    Urgent = false,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Id = 3,
                    ToDoListId = 2,
                    Title = "Do To-Do List application.",
                    Description = "EPAM training",
                    DueDate = DateTime.Now.AddDays(5).Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Id = 4,
                    ToDoListId = 2,
                    Title = "Do First EF Core application.",
                    Description = "EPAM training",
                    DueDate = DateTime.Now.Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Completed,
                },
                new ToDoListTask
                {
                    Id = 5,
                    ToDoListId = 2,
                    Title = "Do ADO.NET application.",
                    Description = "EPAM training",
                    DueDate = DateTime.Now.AddDays(5).Date,
                    Important = true,
                    Urgent = true,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Id = 6,
                    ToDoListId = 3,
                    Title = "Run a marathon.",
                    Description = "everything is possible",
                    DueDate = DateTime.Now.AddYears(1).Date,
                    Important = true,
                    Urgent = false,
                    TaskStatus = Status.Completed,
                },
                new ToDoListTask
                {
                    Id = 7,
                    ToDoListId = 3,
                    Title = "See the Grand Canyon.",
                    Description = "at first think of a plan",
                    DueDate = DateTime.Now.AddYears(2).Date,
                    Important = true,
                    Urgent = false,
                    TaskStatus = Status.Created,
                },
                new ToDoListTask
                {
                    Id = 8,
                    ToDoListId = 3,
                    Title = "Buy a fishing rod.",
                    Description = "2 is better",
                    DueDate = DateTime.Now.AddDays(20).Date,
                    Important = false,
                    Urgent = false,
                    TaskStatus = Status.Created,
                },
            };

            var act = (await this.toDoTaskSevices.GetAllTasksAsync()).ToList();

            if (act.Count != expectedTasks.Count)
            {
                Assert.Fail();
            }

            for (int i = 0; i < expectedTasks.Count; i++)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(expectedTasks[i].Id, Is.EqualTo(act[i].Id));
                    Assert.That(expectedTasks[i].Important, Is.EqualTo(act[i].Important));
                    Assert.That(expectedTasks[i].Description, Is.EqualTo(act[i].Description));
                    Assert.That(expectedTasks[i].DueDate, Is.EqualTo(act[i].DueDate));
                    Assert.That(expectedTasks[i].TaskStatus, Is.EqualTo(act[i].TaskStatus));
                    Assert.That(expectedTasks[i].Title, Is.EqualTo(act[i].Title));
                    Assert.That(expectedTasks[i].ToDoListId, Is.EqualTo(act[i].ToDoListId));
                    Assert.That(expectedTasks[i].Urgent, Is.EqualTo(act[i].Urgent));
                });
            }
        }

        /// <summary>
        /// Test for receiving all tasks.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetAllLists_Lists_ReturnsDate()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            List<ToDoList> expectedLists = new List<ToDoList>
            {
                new ToDoList
                {
                    Id = 1,
                    Title = "Home",
                    Description = "home & family",
                    UserId = "test",
                },
                new ToDoList
                {
                    Id = 2,
                    Title = "Job",
                    Description = "job & career",
                    UserId = "test",
                },
                new ToDoList
                {
                    Id = 3,
                    Title = "Hobby",
                    Description = "favorite activities & health",
                    UserId = "test",
                },
            };

            var act = (await this.toDoListSevices.GetAllListsAsync()).ToList();

            if (act.Count != expectedLists.Count)
            {
                Assert.Fail();
            }

            for (int i = 0; i < expectedLists.Count; i++)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(expectedLists[i].Id, Is.EqualTo(act[i].Id));
                    Assert.That(expectedLists[i].Title, Is.EqualTo(act[i].Title));
                    Assert.That(expectedLists[i].Description, Is.EqualTo(act[i].Description));
                });
            }
        }

        /// <summary>
        ///  Checking the generation of a string with a serial number.
        /// </summary>
        /// <param name="inputString">input String.</param>
        /// <returns>expected value.</returns>
        [TestCase("Hello", ExpectedResult = "Hello(1)")]
        [TestCase("Hello(1)", ExpectedResult = "Hello(2)")]
        public string GenerateStringWithSerialNumber_inputString_expectedString(string inputString)
        {
            return ServicesHelpers.GenerateStringWithSerialNumber(inputString);
        }

        /// <summary>
        /// Checking todoList cloning.
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task ToDoListClone_ToDoList_ClonedList()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            ToDoList arr = this.listRepository.Lists[0];
            List<ToDoListTask> arrTasks = arr.ToDoListTasks.OrderBy(x => x.Title).ToList();

            ToDoList act = (ToDoList)arr.Clone();
            List<ToDoListTask> actTasks = act.ToDoListTasks.OrderBy(x => x.Title).ToList();
            Assert.Multiple(() =>
            {
                Assert.That(arr.Description, Is.EqualTo(act.Description));
                Assert.That(arr.Hide, Is.EqualTo(act.Hide));
                Assert.That(arr.Title, Is.EqualTo(act.Title));
                var cnt = act.ToDoListTasks.Count;
                Assert.That(arr.ToDoListTasks, Has.Count.EqualTo(cnt));
            });
            for (int i = 0; i < arrTasks.Count; i++)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(arrTasks[i].Id, Is.EqualTo(actTasks[i].Id));
                    Assert.That(arrTasks[i].Description, Is.EqualTo(actTasks[i].Description));
                    Assert.That(arrTasks[i].DueDate, Is.EqualTo(actTasks[i].DueDate));
                    Assert.That(arrTasks[i].Important, Is.EqualTo(actTasks[i].Important));
                    Assert.That(arrTasks[i].Remind, Is.EqualTo(actTasks[i].Remind));
                    Assert.That(arrTasks[i].TaskStatus, Is.EqualTo(actTasks[i].TaskStatus));
                    Assert.That(arrTasks[i].Title, Is.EqualTo(actTasks[i].Title));
                    Assert.That(arrTasks[i].Urgent, Is.EqualTo(actTasks[i].Urgent));
                });
            }
        }

        /// <summary>
        /// Checking todoList cloning.
        /// </summary>
        /// <param name="title">title.</param>
        /// <param name="exceptionTitle">exception Title.</param>
        /// <returns>Task.</returns>
        [TestCase("Title", "Title(1)")]
        [TestCase("Long Title lenght 30 characte", "Long Title lenght 30 characte(")]
        public async Task CopyList_List_CopiedList(string title, string exceptionTitle)
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            ToDoList arr = this.listRepository.Lists[0];
            arr.ToDoListTasks = (await this.taskRepository.GetAllAsync()).Where(x => x.ToDoListId == arr.Id).ToList();

            arr.Title = title;

            var newId = await this.toDoListSevices.CopyListAsync(arr.Id);

            var act = (await this.toDoListSevices.GetAllListsAsync()).FirstOrDefault(x => x.Id == newId);

            List<ToDoListTask> arrTasks = arr.ToDoListTasks.OrderBy(x => x.Title).ToList();
            List<ToDoListTask> actTasks = act.ToDoListTasks.OrderBy(x => x.Title).ToList();

            Assert.Multiple(() =>
            {
                Assert.That(act.Title, Is.EqualTo(exceptionTitle));
                Assert.That(arr.Description, Is.EqualTo(act.Description));
                Assert.That(arr.Hide, Is.EqualTo(act.Hide));
            });
            for (int i = 0; i < arrTasks.Count; i++)
            {
                Assert.Multiple(() =>
                {
                    Assert.That(arrTasks[i].Description, Is.EqualTo(actTasks[i].Description));
                    Assert.That(arrTasks[i].DueDate, Is.EqualTo(actTasks[i].DueDate));
                    Assert.That(arrTasks[i].Important, Is.EqualTo(actTasks[i].Important));
                    Assert.That(arrTasks[i].Remind, Is.EqualTo(actTasks[i].Remind));
                    Assert.That(arrTasks[i].TaskStatus, Is.EqualTo(actTasks[i].TaskStatus));
                    Assert.That(arrTasks[i].Title, Is.EqualTo(actTasks[i].Title));
                    Assert.That(arrTasks[i].Urgent, Is.EqualTo(actTasks[i].Urgent));
                });
            }
        }

        /// <summary>
        /// List deletion check.
        /// </summary>
        /// <returns>Task.</returns>
        [Test]
        public async Task DeleteTask_ListId_TaskDeleted()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var id = (await this.listRepository.GetAllAsync()).FirstOrDefault().Id;

            await this.toDoListSevices.DeleteListAsync(id);

            var act = (await this.listRepository.GetAllAsync()).Where(x => x.Id == id).Count();

            Assert.That(act, Is.EqualTo(0));
        }

        /// <summary>
        /// Task deletion check.
        /// </summary>
        /// <returns>Task.</returns>
        [Test]
        public async Task DeleteList_ListId_ListDeleted()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var id = (await this.taskRepository.GetAllAsync()).FirstOrDefault().Id;

            await this.toDoTaskSevices.DeleteTaskAsync(id);

            var act = (await this.taskRepository.GetAllAsync()).Where(x => x.Id == id).Count();

            Assert.That(act, Is.EqualTo(0));
        }

        /// <summary>
        /// SetReminder checking. 
        /// </summary>
        /// <returns>representing the asynchronous unit test.</returns>
        [Test]
        public async Task SetReminder_Input_Output()
        {
            await SeedDate.SeedAsync(this.listRepository, this.taskRepository, "test");

            var task = (await this.taskRepository.GetAllAsync()).FirstOrDefault();

            await this.toDoTaskSevices.SetReminderAsync(task.Id, DateTime.Now.Date);

            var act = (await this.taskRepository.GetAllAsync()).FirstOrDefault(x => x.Id == task.Id).Remind;

            Assert.That(DateTime.Now.Date, Is.EqualTo(act));
        }
    }
}
