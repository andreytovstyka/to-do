﻿namespace Todo_domain_entities
{
    /// <summary>
    /// https://todoist.com/productivity-methods/eisenhower-matrix#quadrant-1-urgent-and-important.
    /// </summary>
    public static class EisenhowerMatrix
    {
        /// <summary>
        /// Choosing advice for a task.
        /// </summary>
        /// <param name="important">important: true or false.</param>
        /// <param name="urgent">urgent: true or false.</param>
        /// <returns>Eisenhower tip.</returns>
        public static string Tip(bool important, bool urgent)
        {
            string tip = "tip: ";

            switch (important, urgent)
            {
                case (true, true):
                    return tip + "DO IT. Task must be completed immediately.";
                case (true, false):
                    return tip + "SCHEDULE IT. the task must be scheduled on your calendar.";
                case (false, true):
                    return tip + "DELEGATE IT. Delegate unimportant tasks to someone else.";
                default:
                    return tip + "DELETE IT. Cancel the task.";
            }
        }
    }
}
