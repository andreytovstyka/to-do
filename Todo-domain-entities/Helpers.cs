﻿namespace Todo_domain_entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;

    /// <summary>
    /// helpers.
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// Checking if the passed value is valid or not.
        /// </summary>
        /// <typeparam name="T">class.</typeparam>
        /// <typeparam name="T1">value type.</typeparam>
        /// <param name="propertyName">property name.</param>
        /// <param name="value">value.</param>
        /// <exception cref="ArgumentException">if value is not valid.</exception>
        public static void IfInvalidCustomAttributesException<T, T1>(string propertyName, T1 value) 
            where T : class
        {
            PropertyInfo? property = typeof(T).GetProperty(propertyName);

            if (property == null)
            {
                throw new ArgumentException($"{propertyName} does not exist.");
            }

            var attributes = property.GetCustomAttributes<ValidationAttribute>(true);
            foreach (ValidationAttribute attribute in attributes)
            {
                if (!attribute.IsValid(value))
                {
                    throw new ArgumentException(attribute.FormatErrorMessage(propertyName));
                }
            }
        }

        /// <summary>
        /// exception if string is empty or null.
        /// </summary>
        /// <param name="value">parameter.</param>
        /// <param name="parameterName">parameter name.</param>
        /// <param name="minValue">minimum string length.</param>
        /// <param name="maxValue">maximum string length.</param>
        /// <exception cref="ArgumentOutOfRangeException">if the string length is less than the minimum value or greater than the maximum value.</exception>
        /// <exception cref="ArgumentException">if the minimum parameter greater than the maximum parameter.</exception>
        public static void IfStringLengthOutOfRangeException(string value, string parameterName, int minValue, int maxValue)
        {
            IfNullOrEmptyException(value, "value");
            IfNullOrEmptyException(parameterName, "parameterName");

            IfFirstGraterThanSecondException(minValue, maxValue);

            if (value.Length < minValue || value.Length > maxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(value), $"{parameterName} must be greater than or equal to {minValue} and less than or equal to {maxValue}");
            }
        }

        /// <summary>
        /// Check if the first value grater than second.
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <exception cref="ArgumentException">if the minimum parameter greater than the maximum parameter.</exception>
        public static void IfFirstGraterThanSecondException(int first, int second)
        {
            if (first > second)
            {
                throw new ArgumentException("first value can not be grater than second value.");
            }
        }

        /// <summary>
        /// exception if string is empty or null.
        /// </summary>
        /// <param name="value">parameter.</param>
        /// <param name="parameterName">parameter name.</param>
        /// <exception cref="ArgumentNullException">if parameter is null.</exception>
        /// <exception cref="ArgumentException">if parameter is empty.</exception>
        public static void IfNullOrEmptyException(string? value, string parameterName)
        {
            IfNullException(value, parameterName);

            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException($"{parameterName} can not be empty");
            }
        }

        /// <summary>
        /// cheking if date today or future.
        /// </summary>
        /// <param name="value">date.</param>
        /// <returns>true if date today or future.</returns>
        public static bool IsDateTodayOrFuture(DateTime value)
        {
            return value.Date >= DateTime.Today;
        }

        /// <summary>
        /// exception if date in the past or null.
        /// </summary>
        /// <param name="value">date.</param>
        /// <param name="parameterName">parameter name.</param>
        /// <exception cref="ArgumentException">if date in the past.</exception>
        public static void IfDatePastException(DateTime value, string parameterName)
        {
            if (!IsDateTodayOrFuture(value))
            {
                throw new ArgumentException($"{parameterName} can not be in the past");
            }
        }

        /// <summary>
        /// exception if null.
        /// </summary>
        /// <typeparam name="T">type.</typeparam>
        /// <param name="value">value.</param>
        /// <param name="parameterName">parameter name.</param>
        /// <exception cref="ArgumentNullException">if value is null.</exception>
        public static void IfNullException<T>(T? value, string parameterName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(parameterName), $"{parameterName} can not be null");
            }
        }

        /// <summary>
        /// Gets min and max lenght of the string specified in the attribute or default.
        /// </summary>
        /// <typeparam name="T">class.</typeparam>
        /// <param name="propertyName">property name.</param>
        /// <returns>min and max lenght of string specified in attribute or default.</returns>
        /// <exception cref="ArgumentOutOfRangeException">if min. length is greater than max, or min or max lenght less then 0.</exception>
        /// <exception cref="ArgumentException">if property does not exist.</exception>
        public static (int minLenght, int maxLenght) GetMinAndMaxValueOfStringLengthAttribute<T>(string propertyName)
            where T : class
        {
            int min, max = int.MaxValue;

            if (string.IsNullOrEmpty(propertyName))
            {
                return (0, max);
            }

            PropertyInfo? propertyInfo = typeof(T).GetProperty(propertyName);

            if (propertyInfo == null)
            {
                throw new ArgumentException($"{propertyName} does not exist.");
            }

            CustomAttributeData? stringLengthAttribute = propertyInfo.CustomAttributes.FirstOrDefault(x => x.AttributeType.Name == "StringLengthAttribute");

            if (stringLengthAttribute == null)
            {
                return (0, max);
            }

            var maxObj = stringLengthAttribute.ConstructorArguments.FirstOrDefault().Value;

            max = (int)maxObj!;

            var minObj = stringLengthAttribute.NamedArguments.FirstOrDefault(x => x.MemberName == "MinimumLength").TypedValue.Value;

            if (minObj == null)
            {
                return (0, max);
            }

            min = (int)minObj;

            IfFirstGraterThanSecondException(min, max);

            if (max < 0)
            {
                throw new ArgumentOutOfRangeException(propertyName, "The maximum length cannot be less than 0");
            }

            if (min < 0)
            {
                throw new ArgumentOutOfRangeException(propertyName, "The minimum length cannot be less than 0");
            }

            return (min, max);
        }
    }
}
