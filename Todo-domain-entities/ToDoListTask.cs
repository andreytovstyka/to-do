﻿namespace Todo_domain_entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;
    using System.Reflection;

    /// <summary>
    /// ToDoList task.
    /// </summary>
    public class ToDoListTask : ToDoEntityBase
    {
        private bool important;
        private bool urgent;
        private DateTime dueDate = DateTime.Today;
        private DateTime? remind;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListTask"/> class.
        /// </summary>
        public ToDoListTask()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoListTask"/> class.
        /// </summary>
        /// <param name="toDoListId">id ToDoList.</param>
        /// <param name="title">title.</param>
        /// <param name="description">description.</param>
        /// <param name="dueDate">due Date.</param>
        /// <param name="important">important.</param>
        /// <param name="urgent">urgent.</param>
        /// <param name="status">status.</param>
        /// <param name="remind">remind.</param>
        /// <exception cref="ArgumentNullException">if title or description or id or dueDate or important or urgent or status is null.</exception>
        /// <exception cref="ArgumentException">if title or description is empty.</exception>
        public ToDoListTask(long toDoListId, string title, string description, DateTime dueDate, bool important, bool urgent, Status status, DateTime? remind = null)
        {
            this.ToDoListId = toDoListId;
            this.Title = title;
            this.Description = description;
            this.DueDate = dueDate;
            this.TaskStatus = status;
            this.Important = important;
            this.Urgent = urgent;
            this.Remind = remind;
        }

        /// <summary>
        /// Gets or sets Status.
        /// </summary>
        public Status TaskStatus { get; set; } = Status.Created;

        /// <summary>
        /// Gets or sets Due date.
        /// </summary>
        /// <exception cref="ArgumentException">if date in the past.</exception>
        [Display(Name = "Due date")]
        [Required(ErrorMessage = "Please enter a due date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        [CustomDateAttribute(ErrorMessage = "Due date can not be in the past")]
        public DateTime DueDate
        {
            get => this.dueDate.Date;
            set
            {
                var propertyName = MethodBase.GetCurrentMethod() !.Name.Replace("set_", string.Empty, StringComparison.Ordinal);
                Helpers.IfNullException(value, propertyName);

                if (this.Id == 0)
                {
                    Helpers.IfInvalidCustomAttributesException<ToDoListTask, DateTime>(propertyName!, value);
                }

                this.dueDate = value.Date;
            }
        }

        /// <summary>
        /// Gets or sets Remind datetime.
        /// </summary>
        [Display(Name = "Reminder date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:D}")]
        public DateTime? Remind
        {
            get
            {
                if (this.remind != null)
                {
                    return this.remind.Value.ToLocalTime();
                }

                return null;
            }

            set
            {
                this.remind = value;

                if (value is not null)
                {
                    this.remind = ((DateTime)value).ToUniversalTime();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether important true or false.
        /// </summary>
        /// <exception cref="ArgumentNullException">if important is null.</exception>
        public bool Important
        {
            get => this.important;
            set
            {
                Helpers.IfNullException(value, "important");

                this.important = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether urgent true, not urgent false.
        /// </summary>
        /// <exception cref="ArgumentNullException">if urgent is null.</exception>
        public bool Urgent
        {
            get => this.urgent;
            set
            {
                Helpers.IfNullException(value, "urgent");

                this.urgent = value;
            }
        }

        /// <summary>
        /// Gets or sets foreign key.
        /// </summary>
        [ForeignKey(nameof(ToDoList))]
        public long ToDoListId { get; set; }
    }
}
