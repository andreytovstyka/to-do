﻿namespace Todo_domain_entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;

    /// <summary>
    /// Base class.
    /// </summary>
    public abstract class ToDoEntityBase
    {
        private DateTime dateCreation = DateTime.UtcNow;
        private string title = string.Empty;
        private string description = string.Empty;

        /// <summary>
        /// Gets or sets Id To-Do List.
        /// </summary>
        [Required]
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets title of To-Do List.
        /// </summary>
        /// <exception cref="ArgumentNullException">if title is null.</exception>
        /// <exception cref="ArgumentException">if title is empty.</exception>
        /// <exception cref="ArgumentOutOfRangeException">if the title length is less than the minimum value or greater than the maximum value.</exception>
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Title must be between 3 and 30 characters long")]
        [Required(ErrorMessage = "Please enter a title")]
        public string Title
        {
            get => this.title;
            set
            {
                var propertyName = MethodBase.GetCurrentMethod() !.Name.Replace("set_", string.Empty, StringComparison.Ordinal);
                Helpers.IfNullOrEmptyException(value, propertyName);
                Helpers.IfInvalidCustomAttributesException<ToDoEntityBase, string>(propertyName!, value);
                this.title = value;
            }
        }

        /// <summary>
        /// Gets or sets description of To-Do List.
        /// </summary>
        /// <exception cref="ArgumentNullException">if description is null.</exception>
        /// <exception cref="ArgumentException">if description is empty.</exception>
        /// <exception cref="ArgumentOutOfRangeException">if the description length is less than the minimum value or greater than the maximum value.</exception>
        [DataType(DataType.Text)]
        [StringLength(250, MinimumLength = 5, ErrorMessage = "Description must be between 5 and 250 characters long")]
        [Required(ErrorMessage = "Please enter a description")]
        public string Description
        {
            get => this.description;
            set
            {
                var propertyName = MethodBase.GetCurrentMethod() !.Name.Replace("set_", string.Empty, StringComparison.Ordinal);
                Helpers.IfNullOrEmptyException(value, propertyName);
                Helpers.IfInvalidCustomAttributesException<ToDoEntityBase, string>(propertyName!, value);
                this.description = value;
            }
        }

        /// <summary>
        /// Gets or sets creation date.
        /// </summary>
        [Display(Name = "Date of creation")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:D}")]
        public DateTime DateCreation
        {
            get => this.dateCreation.ToLocalTime();
            set
            {
                this.dateCreation = value;
                this.dateCreation = DateTime.UtcNow;
            }
        }
    }
}
