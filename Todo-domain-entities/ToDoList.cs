﻿namespace Todo_domain_entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Reflection;

    /// <summary>
    /// ToDoList.
    /// </summary>
    public class ToDoList : ToDoEntityBase, ICloneable
    {
        private string userId = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoList"/> class.
        /// </summary>
        public ToDoList()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoList"/> class.
        /// </summary>
        /// <param name="title">title.</param>
        /// <param name="description">description.</param>
        /// <param name="userId">user Id.</param>
        /// <exception cref="ArgumentNullException">if title or description is null.</exception>
        /// <exception cref="ArgumentException">if title or description is empty.</exception>
        public ToDoList(string title, string description, string userId)
        {
            this.Title = title;
            this.Description = description;
            this.UserId = userId;
        }

        /// <summary>
        /// Gets or sets a value indicating whether hide list from to-do list or not.
        /// </summary>
        public bool Hide { get; set; }

        /// <summary>
        /// Gets or sets user id.
        /// </summary>
        /// <exception cref="ArgumentNullException">if title is null.</exception>
        /// <exception cref="ArgumentException">if title is empty.</exception>
        /// <exception cref="ArgumentOutOfRangeException">if the title length is less than the minimum value or greater than the maximum value.</exception>
        [MaxLength(450)]
        public string UserId 
        {
            get => this.userId;
            set
            {
                var propertyName = MethodBase.GetCurrentMethod() !.Name.Replace("set_", string.Empty, StringComparison.Ordinal);
                Helpers.IfNullOrEmptyException(value, propertyName);
                Helpers.IfInvalidCustomAttributesException<ToDoList, string>(propertyName!, value);

                this.userId = value;
            }
        }

        /// <summary>
        /// Gets or sets navigation property.
        /// </summary>
        public ICollection<ToDoListTask> ToDoListTasks { get; set; } = new List<ToDoListTask>();

        public virtual object Clone()
        {
            ToDoList newToDoList = new ToDoList();

            newToDoList.Description = this.Description;
            newToDoList.Hide = this.Hide;
            newToDoList.Title = this.Title;
            newToDoList.UserId = this.UserId;

            List<ToDoListTask> newTasks = new List<ToDoListTask>();

            foreach (var task in this.ToDoListTasks)
            {
                ToDoListTask newTask = new ToDoListTask();
                newTask.Description = task.Description;
                try
                {
                    newTask.DueDate = task.DueDate;
                }
                catch (ArgumentException)
                {
                    newTask.DueDate = DateTime.Today;
                }

                newTask.Important = task.Important;
                newTask.TaskStatus = task.TaskStatus;
                newTask.Title = task.Title;
                newTask.Urgent = task.Urgent;

                newTasks.Add(newTask);
            }

            newToDoList.ToDoListTasks = newTasks;

            return newToDoList;
        }
    }
}
