﻿namespace Todo_domain_entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;

    /// <summary>
    /// CustomAttributeDate for Due data.
    /// </summary>
    public class CustomDateAttribute : ValidationAttribute
    {
        /// <summary>
        /// Check if not the past date.
        /// </summary>
        /// <param name="value">date value.</param>
        /// <returns>True, if not the past date.</returns>
        public override bool IsValid(object? value)
        {
            if (value == null)
            {
                return false;
            }

            DateTime dateTime = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            return dateTime >= DateTime.Now.Date;
        }
    }
}
