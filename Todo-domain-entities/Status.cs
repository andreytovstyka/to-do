﻿namespace Todo_domain_entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///  Status of task or list.
    /// </summary>
    public enum Status : byte
    {
        /// <summary>
        /// Created task.
        /// </summary>
        [Display(Name = "Not Started")]
        Created = 0,

        /// <summary>
        /// Started task.
        /// </summary>
        [Display(Name = "In Progress")]
        Started = 1,

        /// <summary>
        /// Completed task.
        /// </summary>
        Completed = 2,
    }
}
